package org.anyline.simple.influx;


import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.InfluxDBClientOptions;
import com.influxdb.client.OrganizationsApi;
import com.influxdb.client.domain.Bucket;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.write.Point;
import org.anyline.entity.DataRow;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class InfluxApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(InfluxApplication.class);
        ConfigurableApplicationContext context = application.run(args);
        DataRow row = context.getBean( DataRow.class);
        System.out.println(row);
     }
}
