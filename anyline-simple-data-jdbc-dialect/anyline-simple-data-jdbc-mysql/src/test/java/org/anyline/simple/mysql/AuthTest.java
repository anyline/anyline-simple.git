package org.anyline.simple.mysql;

import org.anyline.entity.authorize.Privilege;
import org.anyline.entity.authorize.User;
import org.anyline.proxy.ServiceProxy;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest(classes = MySQLApplication.class)
public class AuthTest {
    @Test
    public void user() throws Exception{
        //查询用户
        User query = new User();
        query.setHost("%");
        query.setName("u1");
        List<User> users =  ServiceProxy.authorize().users(query);
        //删除用户
        if(!users.isEmpty()){
            ServiceProxy.authorize().drop(query);
        }
        //创建用户
        User user = new User();
        user.setName("u1");
        user.setHost("%");
        user.setPassword("000000");
        ServiceProxy.authorize().create(user);

        List<User> list = ServiceProxy.authorize().users();

        //授权
        Privilege privilege = new Privilege();
        privilege.setName("ALL");
        privilege.setDatabase("*").setObjectName("*");
        ServiceProxy.authorize().grant(user, privilege);

        privilege = new Privilege();
        privilege.addActions("SELECT", "DELETE", "INSERT", "UPDATE");
        privilege.setDatabase("simple").setObjectName("CRM_USER");
        ServiceProxy.authorize().grant(user, privilege);

        //取消授权
        privilege = new Privilege();
        privilege.addActions("DELETE");
        privilege.setDatabase("simple").setObjectName("CRM_USER");
        ServiceProxy.authorize().revoke(user, privilege);


    }

}
