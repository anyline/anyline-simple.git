package org.anyline.simple.kingbase.entity;


import java.io.Serializable;

public class Log implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;

    /**
     * 操作行为 （1:新增 2:修改 3:删除 ）
     */
    private String action;


    /**
     * 操作的数据库模式
     */
    private String tableMode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getTableMode() {
        return tableMode;
    }

    public void setTableMode(String tableMode) {
        this.tableMode = tableMode;
    }
}

