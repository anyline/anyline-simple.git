package org.anyline.simple.iotdb;

import org.anyline.metadata.Table;
import org.anyline.service.AnylineService;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class IotDBTest {
    private Logger log = LoggerFactory.getLogger(IotDBTest.class);
    @Autowired
    private AnylineService service          ;
    @Test
    public void ddl() throws Exception{
        Table table = service.metadata().table("root.simple.dp.car.v");
        if(null != table){
            service.ddl().drop(table);
        }
    }

}
