package org.anyline.simple.iotdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IOTApplication {
	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(IOTApplication.class);
		application.run(args);
	}
}
