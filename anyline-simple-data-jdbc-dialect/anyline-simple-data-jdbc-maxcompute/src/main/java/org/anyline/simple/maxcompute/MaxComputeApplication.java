package org.anyline.simple.maxcompute;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class MaxComputeApplication {
    public static void main(String[] args){
        SpringApplication application = new SpringApplication(MaxComputeApplication.class);
        application.run(args);
    }
}
