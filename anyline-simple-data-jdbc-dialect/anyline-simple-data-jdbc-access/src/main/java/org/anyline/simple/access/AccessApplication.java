package org.anyline.simple.access;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;


@SpringBootApplication
public class AccessApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(AccessApplication.class);
        ConfigurableApplicationContext ctx = application.run(args);
    }
}
