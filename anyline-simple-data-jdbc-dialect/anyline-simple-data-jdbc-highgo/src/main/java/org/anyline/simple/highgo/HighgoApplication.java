package org.anyline.simple.highgo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HighgoApplication {
    public static void main(String[] args){
        SpringApplication application = new SpringApplication(HighgoApplication.class);
        application.run(args);
    }
}
