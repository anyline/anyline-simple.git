package org.anyline.simple.test;

import org.anyline.data.param.init.DefaultConfigStore;
import org.anyline.entity.DataRow;
import org.anyline.entity.DataSet;
import org.anyline.metadata.Table;
import org.anyline.service.AnylineService;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class IgniteTest {
    private Logger log = LoggerFactory.getLogger(IgniteTest.class);
    @Autowired
    private AnylineService service          ;
    @Test
    public void ddl() throws Exception{
        Table table = service.metadata().table("CRM_TEST");
        if(null != table){
            service.ddl().drop(table);
        }
        table = new Table("CRM_TEST");
        table.addColumn("id","bigint").primary(true);
        table.addColumn("name","varchar(10)");
        service.ddl().create(table);
    }

    @Test
    public void override() throws Exception{
        String tab = "tab_override";
        Table table = service.metadata().table(tab);
        if(null != table){
            service.ddl().drop(table);
        }
        //表必须有主键
        table = new Table(tab);
        table.addColumn("ID", "BIGINT").primary(true);
        table.addColumn("CODE", "varchar(10)");
        service.ddl().create(table);

        DataRow row = new DataRow();
        row.put("ID", 1);
        row.put("CODE", "C2");
        service.insert(tab, row);

        row = new DataRow();
        row.put("ID", 1);
        row.put("CODE", "C2");
        //相同主键覆盖，实际执行MERGE INTO，这时CODE被更新成C2
        service.insert(tab, row, new DefaultConfigStore().override(Boolean.TRUE));
        DataSet set = service.querys(tab);
        System.out.println(set);

        row = new DataRow();
        row.put("ID", 1);
        row.put("CODE", "C2");
        //不覆盖，会生成INSERT,因为重复所以抛出异常
        //service.insert(tab, row, new DefaultConfigStore().override(Boolean.FALSE));
    }
}
