package org.anyline.simple.tdengine;

import org.anyline.data.datasource.DataSourceHolder;
import org.anyline.data.param.ConfigStore;
import org.anyline.data.param.init.DefaultConfigStore;
import org.anyline.entity.DataRow;
import org.anyline.entity.DataSet;
import org.anyline.metadata.*;
import org.anyline.proxy.ServiceProxy;
import org.anyline.service.AnylineService;
import org.anyline.util.ConfigTable;
import org.anyline.util.LogUtil;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@SpringBootTest
public class TdengineTest {
    @Autowired
    private AnylineService service;
    private static Logger log = LoggerFactory.getLogger(TdengineTest.class);
    @Test
    public void query(){
        ConfigTable.IS_AUTO_CHECK_METADATA = true;
        service.querys("s_table_user", "I:1","CODE:1");
    }
    @Test
    public void dc3() throws Exception{
        String url ="jdbc:TAOS-RS://192.168.110.101:6041/dc3?charset=UTF-8&locale=en_US.UTF-8&timezone=UTC-8";
        String driver = "com.taosdata.jdbc.rs.RestfulDriver";
        DataSourceHolder.reg("dc3", "com.zaxxer.hikari.HikariDataSource", driver, url,"root","taosdata");
        ConfigTable.IS_SQL_DELIMITER_OPEN = true;

        MasterTable table = ServiceProxy.service("dc3").metadata().master("device_point_data");
        if(null != table){
            ServiceProxy.service("dc3").drop(table);
        }
        table = new MasterTable("device_point_data");
        table.addColumn("id", "TIMESTAMP");
        table.addTag("device_id", "BIGINT").setComment("设备ID");
        table.addTag("point_id", "BIGINT").setComment("位号ID");
        table.addTag("origin_time", "TIMESTAMP").setComment("原始时间");
        table.addColumn("raw_value", "nchar(20)").setComment("原始值");
        table.addColumn("value", "nchar(20)").setComment("设备处理值");
        table.addColumn("create_time", "TIMESTAMP").setComment("创建时间");
        table.addColumn("operate_time", "TIMESTAMP").setComment("操作时间");
        ServiceProxy.service("dc3").ddl().create(table);

    }
    @Test
    public void master() throws Exception{
        System.out.println("\n-------------------------------- start stable  --------------------------------------------\n");

        LinkedHashMap<String, MasterTable> tables = service.metadata().masters();
        log.warn(LogUtil.format("检索超表数量:"+tables.size(),34));
        for(Table table:tables.values()){
            log.warn("表:"+table.getName());
        }


        MasterTable table = service.metadata().master("s_table_user");
        if(null != table){
            //表存存 查看表结构
            log.warn(LogUtil.format("查询表结构:"+table.getName(),34));
            LinkedHashMap<String, Column> columns = table.getColumns();
            for(Column column:columns.values()){
                log.warn("列:"+column.toString());
            }
            log.warn(LogUtil.format("删除表",34));
            service.ddl().drop(table);
        }
        table = new MasterTable("s_table_user");
        //table.setComment("表备注");
        table.addColumn("ID", "TIMESTAMP");
        table.addColumn("CODE","NCHAR(10)").setComment("编号");

        table.addTag("I", "int").setComment("int tag");
        table.addTag("D", "double");
        table.addTag("S", "nchar(10)");
        table.setName("s_table_user");

        log.warn(LogUtil.format("创建超表",34));
        service.ddl().save(table);

        table.addColumn("age","int");
        table.addColumn("NAME","NCHAR(10)");
        table.addTag("NT", "int");
        log.warn(LogUtil.format("超表添加列与标签",34));
        service.ddl().save(table);
        //创建子表
        for(int i=0;i < 10;i++){
            PartitionTable item = new PartitionTable();
            item.setName("s_table_user_"+i);
            item.setMaster("s_table_user");
            item.addTag("I", null,i);
            item.addTag("D", null, 10);
            item.addTag("S", null, "S"+i);
            log.warn(LogUtil.format("删除子表",34));
            service.ddl().drop(item);
            log.warn(LogUtil.format("创建子表",34));
            service.ddl().create(item);
        }

        log.warn(LogUtil.format("根据主表查询子表",34));
        LinkedHashMap<String,PartitionTable> items = service.metadata().partitions(table);
        for(Table item:items.values()){
            log.warn("子表:"+item.getName());
        }
        log.warn(LogUtil.format("根据主表与标签值查询子表",34));
        Map<String,Object> tags = new HashMap<>();
        tags.put("I", 1);
        tags.put("D",10);
        tags.put("S", "S1");
        items = service.metadata().partitions(table, tags);
        for(Table item:items.values()){
            log.warn("子表:"+item.getName());
        }

        DataSet set = new DataSet();
        long fr = System.currentTimeMillis();
        for(int i=0; i<10; i++){
            DataRow row = new DataRow();
            row.put("ID", fr+i);
            row.put("CODE","C"+i);
            row.put("age", i*10);
            set.add(row);
        }


        log.warn(LogUtil.format("直接插入子表",34));
        service.insert("s_table_user_1", set);

        System.out.println("\n-------------------------------- end stable  --------------------------------------------\n");
    }

    @Test
    public void table() throws Exception{
        System.out.println("\n-------------------------------- start table  --------------------------------------------\n");
        LinkedHashMap<String,Table> tables = service.metadata().tables();
        log.warn(LogUtil.format("检索表数量:"+tables.size(),34));
        int qty = 0;
        for(Table table:tables.values()){
            log.warn("表:"+table.getName());
            if(qty++ > 10){
                break;
            }
        }


        Table table = service.metadata().table("s_table_user");
        if(null != table){
            service.ddl().drop(table);
        }

        //数据类型参考这里 https://docs.taosdata.com/taos-sql/data-type/
        log.warn(LogUtil.format("创建表,第一个字段必须是 TIMESTAMP，并且系统自动将其设为主键",34));
        table = new Table("s_table_user");
        //第一个字段必须是 TIMESTAMP，并且系统自动将其设为主键
        table.addColumn("ID", "TIMESTAMP");
        //用NCHAR  不要用 VARCHAR(BINARY类型的别名)
        table.addColumn("CODE","NCHAR(10)").setComment("编号");
        table.addColumn("age","int");
        service.ddl().save(table);

        DataSet set = new DataSet();
        long id = System.currentTimeMillis();
        for(int i=0; i<10; i++){
            DataRow row = set.add();
            row.put("ID", id+i);
            row.put("CODE", "C"+i);
            row.put("age", i+1);
        }
        ConfigStore configs = new DefaultConfigStore();
        configs.IS_SQL_LOG_PLACEHOLDER(false);
        service.insert(table, set, configs);
        set = service.querys(table);
        table = service.metadata().table("s_table_user");
        if(null != table){
            log.warn(LogUtil.format("查询表结构:"+table.getName(),34));
            LinkedHashMap<String,Column> columns = table.getColumns();
            for(Column column:columns.values()){
                log.warn("列:"+column.toString());
            }
            log.warn(LogUtil.format("删除表",34));
            service.ddl().drop(table);
        }

        System.out.println("\n-------------------------------- end table  --------------------------------------------\n");
    }

    @Test
    public void mtable() throws Exception{
        System.out.println("\n-------------------------------- start stable  --------------------------------------------\n");

        LinkedHashMap<String,MasterTable> tables = service.metadata().masters();
        log.warn(LogUtil.format("检索超表数量:"+tables.size(),34));
        for(Table table:tables.values()){
            log.warn("表:"+table.getName());
        }


        MasterTable table = service.metadata().master("s_table_user");
        if(null != table){
            //表存存 查看表结构
            log.warn(LogUtil.format("查询表结构:"+table.getName(),34));
            LinkedHashMap<String,Column> columns = table.getColumns();
            for(Column column:columns.values()){
                log.warn("列:"+column.toString());
            }
            log.warn(LogUtil.format("删除表",34));
            service.ddl().drop(table);
        }else{
            //表不存在  创建新表
            table = new MasterTable("s_table_user");

        }
        //table.setComment("表备注");
        table.addColumn("ID", "TIMESTAMP");
        table.addColumn("CODE","NCHAR(10)").setComment("编号");
        table.addColumn("VAL","INT").setComment("编号");

        table.addTag("I", "int").setComment("int tag");
        table.addTag("D", "double");
        table.addTag("S", "nchar(10)");
        table.setName("s_table_user");

        log.warn(LogUtil.format("创建超表",34));
        service.ddl().save(table);

        table.addColumn("age","int");
        table.addColumn("NAME","NCHAR(10)");
        table.addTag("NT", "int");
        log.warn(LogUtil.format("超表添加列与标签",34));
        service.ddl().save(table);
        //创建子表
        for(int i=0;i < 10; i++){
            PartitionTable item = new PartitionTable();
            item.setName("s_table_user_"+i);
            item.setMaster("s_table_user");
            item.addTag("I", null,i);
            item.addTag("D", null, 10);
            item.addTag("S", null, "S"+i);
            log.warn(LogUtil.format("删除子表",34));
            service.ddl().drop(item);
            log.warn(LogUtil.format("创建子表",34));
            service.ddl().create(item);
        }

        log.warn(LogUtil.format("根据主表查询子表",34));
        LinkedHashMap<String,PartitionTable> items = service.metadata().partitions(table);
        for(Table item:items.values()){
            log.warn("子表:"+item.getName());
        }
        log.warn(LogUtil.format("根据主表与标签值查询子表",34));
        Map<String,Object> tags = new HashMap<>();
        tags.put("I", 1);
        tags.put("D",10);
        tags.put("S", "S1");
        items = service.metadata().partitions(table, tags);
        for(Table item:items.values()){
            log.warn("子表:"+item.getName());
        }

        //插入子表 并检测子表是否存在 不存在则创建新表
        DataSet set = new DataSet();
        Long fr = System.currentTimeMillis();
        for(int i=0; i<10; i++){
            DataRow row = set.add();
            row.put("ID", fr+i);
            row.put("CODE", "C"+i);
            row.put("VAL", i);
        }
        PartitionTable partition = new PartitionTable();
        partition.setName("s_table_user_new");  //插入和创建的分区表
        partition.setMaster(table); //设置对应的主表
        partition.addTag(new Tag("I", 1));  //设置TAG值
        partition.addTag(new Tag("d", 2));
        service.insert(partition, set);
        System.out.println("\n-------------------------------- end stable  --------------------------------------------\n");
    }
}
