package org.anyline.simiple.nebula;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;


@SpringBootApplication
public class NebulaApplication {
    private static Logger log = LoggerFactory.getLogger(NebulaApplication.class);
    public static void main(String[] args) throws Exception{
        SpringApplication application = new SpringApplication(NebulaApplication.class);
        application.run(args);
    }
}
