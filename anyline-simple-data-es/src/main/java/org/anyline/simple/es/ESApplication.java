package org.anyline.simple.es;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ESApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(ESApplication.class);
        application.run(args);
    }

}
