package org.anyline.simple.office.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller("web.home.DefaultController")
@RequestMapping("/")
public class DefaultController extends BasicController {

    @RequestMapping({"","index"})
    public ModelAndView index( ) {

        ModelAndView mv = template("index.jsp");
        return mv;
    }

}
