package org.anyline.simple.office;


import org.anyline.service.AnylineService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication


public class OfficeApplication extends SpringBootServletInitializer {
	private static Logger log = LoggerFactory.getLogger(OfficeApplication.class);
	private static AnylineService service;
	public static void main(String[] args) {

		SpringApplication application = new SpringApplication(OfficeApplication.class);
		application.run(args);

	}

}
