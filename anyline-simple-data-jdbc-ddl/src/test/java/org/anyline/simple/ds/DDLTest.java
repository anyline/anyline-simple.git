package org.anyline.simple.ds;

import org.anyline.data.adapter.DriverAdapter;
import org.anyline.data.jdbc.adapter.JDBCAdapter;
import org.anyline.data.jdbc.mysql.MySQLAdapter;
import org.anyline.data.jdbc.oracle.OracleAdapter;
import org.anyline.data.param.ConfigStore;
import org.anyline.data.param.init.DefaultConfigStore;
import org.anyline.data.run.Run;
import org.anyline.data.runtime.DataRuntime;
import org.anyline.data.runtime.RuntimeHolder;
import org.anyline.entity.DataRow;
import org.anyline.entity.DataSet;
import org.anyline.metadata.*;
import org.anyline.proxy.ServiceProxy;
import org.anyline.service.AnylineService;
import org.anyline.util.BasicUtil;
import org.anyline.util.ConfigTable;
import org.anyline.util.DateUtil;
import org.anyline.util.LogUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalTime;
import java.util.*;

@SpringBootTest(classes = DDLApplication.class)
public class DDLTest {
    private Logger log = LoggerFactory.getLogger(DDLTest.class);
    @Autowired
    private AnylineService service          ;
    @Test
    public void start() throws Exception{
        check(null, "MySQL");
        check("pg", "PostgreSQL");
        check("ms", "SQL Server");
        check("ms2000", "SQL Server 2000");
        check("oracle", "Oracle 11G");
        check("db2", "DB2");
        check("kb", "KingBase");
        check("doris", "doris");
    }
    @Test
    public void pg2doris() throws Exception {
        Table table = service.metadata().table("tab_time");
        table.setEngine("olap");
        table.setProperty("replication_allocation", "tag.location.default:1");
        //设置分桶方式 DISTRIBUTED BY HASH('ID'） BUCKETS 2
        table.setDistribution(Table.Distribution.TYPE.HASH, 3, "RYBM");
        ServiceProxy.service("doris").ddl()
            .create(table);

    }
    @Test
    public void mysql2clickhouse() throws Exception {
        Table table = service.metadata().table("dwd_iot_vehicle_info12");

        table.setEngine("MergeTree()");
        table.setCharset(null);
        table.setCollate(null);
        ServiceProxy.service("click").ddl().create(table);

    }
    public void check(String ds, String title) throws Exception{
        System.out.println("\n=============================== START " + title + "=========================================\n");
        if(null != ds) {
            service = ServiceProxy.service(ds);
        };
        type_param();
       /* type();
        table();
        view();
        column();
        index();
        pk();
        exception();
        foreign();
        trigger();
        clear();
        all();*/
        System.out.println("\n=============================== END " + title + "=========================================\n");
    }


    @Test
    public void mysql2mssql() throws Exception{
        Table table = init("CRM_USER");
        AnylineService<?> service = ServiceProxy.service();
        table = service.metadata().table("CRM_USER");
        table.setSchema("dbo");
        table.setName("mysql2mssql_crm_user");
        table.setEngine(null);
        System.out.println("output=================");
        org.slf4j.Logger s;
        ServiceProxy.service("ms").ddl().create(table);
    }
    @Test
    public void mysql2oracle() throws Exception{
        Table table = init("CRM_USER");
        table = service.metadata().table("CRM_USER");
        table.setCatalog((String)null);
        table.setSchema((String)null);
        table.setName("mysql2oracle_crm_user");
        ServiceProxy.service("oracle").ddl().create(table);
    }
    @Test
    public void add_column() throws Exception {
        Table table = service.metadata().table("crm_user");
        table.addColumn("IDS_"+System.currentTimeMillis(),"bigint", true, 1).setComment("test");
        Column col = new Column();
        col.setName("ADD_"+System.currentTimeMillis());
        col.setType("bigint");
        col.setNullable(false);
        col.setLength(10);
        col.setAutoIncrement(true);
        col.setPrimary(false);
        table.addColumn(col);
        Column col2 = new Column();
        col2.setName("ADD2_"+System.currentTimeMillis());
        col2.setType("varchar");
        col2.setNullable(false);
        col2.setLength(10);
        table.addColumn(col2);
        Column col3 = new Column();
        col3.setName("ADD3_"+System.currentTimeMillis());
        col3.setType("decimal");
        col3.setNullable(false);
        col3.setLength(10);
        col3.setScale(2);
        table.addColumn(col3);
        Column col4 = new Column();
        col4.setName("ADD4_"+System.currentTimeMillis());
        col4.setType("decimal");
        col4.setNullable(false);
        col4.setPrecision(10);
        col4.setScale(2);
        table.addColumn(col4);
        service.ddl().save(table);
    }

    @Test
    public void drop_index() throws Exception{
        Table tab = init("CRM_USER");
        Index idx = tab.getIndex("IDX_ID_CODE_NAME");
        if(null == idx){
            idx = new Index("IDX_ID_CODE_NAME");
            idx.addColumn("ID", "ASC");
            idx.addColumn("CODE", "DESC");
            idx.addColumn("NAME", "DESC");
            idx.setTable(tab);
            service.ddl().add(idx);
        }
        tab = service.metadata().table("CRM_USER");
        LinkedHashMap<String, Index> indexes = tab.getIndexes();
        for(Index index:indexes.values()){
            System.out.println("\nindex:"+index.getName());
            LinkedHashMap<String, Column> columns = index.getColumns();
            for(Column column:columns.values()){
                System.out.println("column:"+column.getName() + " position:"+index.getPosition(column.getName()) + " order:"+index.getOrder(column.getName()));
            }
        }
        idx = tab.getIndex("IDX_ID_CODE_NAME");
        idx.drop();
        service.ddl().save(tab);
    }
    @Test
    public void double_test() throws Exception{
        Table table = new Table("test_"+System.currentTimeMillis());
        //table.addColumn("ID", "INT");
        //table.addColumn("price1", "float(10,2)");
        table.addColumn("price2", "double(10)");
        service.ddl().create(table);
    }
    @Test
    public void type_param() throws Exception{

        type("int");            //int          正常
        type("int(10)");        //int(10)      异常

        type("float");          //float        正常
        type("float(32)");      //float(32)    正常
        type("float(10)");      //float(10)    正常
        type("float(10,2)");    //float(10,2)  异常
        type("double");         //double       正常
        type("double(32)");     //double(32)   异常
        type("double(10)");     //double(10)   异常
        type("double(10,2)");   //double(10,2) 异常
    }
    private void type(String type){
        try {
            String sql = "CREATE TABLE TAB_" + System.currentTimeMillis() + "(id int, code "+type+")";
            service.execute(sql);
            System.out.println(LogUtil.format(type + " 正常", 32));
        }catch (Exception e){
            System.out.println(LogUtil.format(type + " 异常", 31));
        }
    }
    @Test
    public void db2_double() throws Exception{
        service = ServiceProxy.service("db2");
        String name ="db2_double";
        ConfigTable.IS_THROW_SQL_UPDATE_EXCEPTION = true; //遇到SQL异常直接抛出
        //检测表结构
        Table table = service.metadata().table(name);
        //如果存在则删除
        if (null != table) {
            service.ddl().drop(table);
        }

        //定义表结构
        table = new Table(name);

        //添加列
        //自增长列 如果要适配多种数据库 autoIncrement 有必须的话可以设置起始值与增量 autoIncrement(int seed, int step)
        table.addColumn("DATA_D1", "double(10,1)", false, 1.1).setComment("数据版本");
        table.addColumn("DATA_D2", "double", false, 1.1).setPrecision(10).setScale(2);
        table.addColumn("DATA_F1", "FLOAT(10,1)", false, 1.1).setComment("数据版本");
        table.addColumn("DATA_F2", "FLOAT", false, 1.1).setPrecision(10).setScale(2);

        //创建表
        service.ddl().create(table);
    }
    @Test
    public void primary_alter() throws Exception{

        Table table = service.metadata().table("tab_pk", false);
        if(table != null){
            service.ddl().drop(table);
        }
        table = new Table("tab_pk");
        table.addColumn("ID", "int4").setAutoIncrement(true).setPrimary(true);
        service.ddl().create(table);
        table.getColumn("id").setComment("comment");
        table.addColumn("V", "varchar(10)").setDefaultValue("1");
        service.ddl().save(table);

    }
    @Test
    public void index_alter() throws Exception{
        Table table = service.metadata().table("test_index2", false);
        if(table != null){
            service.ddl().drop(table);
        }
        table = new Table("test_index2");
        table.setComment("表备注");
        Column column = new Column();
        column.setName("id");
        column.setType("int4");
        column.setAutoIncrement(true);
        column.setPrimary(true);
        table.addColumn(column);

        Column column2 = new Column();
        column2.setName("name");
        column2.setType("varchar(255)");
        column2.setComment("test111");
        table.addColumn(column2);

        table.addColumn("C1", "INT(12)");
        table.addColumn("C2", "INT").setLength(10);
        table.addColumn("C12", "decimal").setLength(10);
        table.addColumn("C3", "VARCHAR").setPrecision(255);
        boolean save = service.ddl().save(table);

//新增索引
        Index index = new Index();
        index.setName("index_a");
        index.setUnique(true);
        index.setTable(table);
        index.addColumn("id");
        boolean add = service.ddl().add(index);

//修改索引
        Table tableUpdate = service.metadata().table("test_index2");
        Index indexUpdate = tableUpdate.getIndex(index.getName());
        LinkedHashMap columns = tableUpdate.getColumns();
        indexUpdate.setColumns(columns);
        boolean alter = service.ddl().alter(indexUpdate);
        log.info("索引修改：{}", alter);
    }
    @Test
    public void mysql2doris() throws Exception{

        Table table = service.metadata().table("t_doris");
        if(null != table){
            service.ddl().drop(table);
        }
        table = new Table("t_doris");
        table.addColumn("ID","INT").setPrimary(true).setAutoIncrement(true);
        table.addColumn("code","number(10,2)");
        table.addColumn("BTS", "BLOB");
        service.ddl().create(table);
        table = service.metadata().table("t_doris");
        ServiceProxy.service("doris").ddl().create(table);
    }
    @Test
    public void alter() throws Exception{
        Table table = service.metadata().table("tab_alter", false);
        if(null != table){
            service.ddl().drop(table);
        }
        table = service.metadata().table("tab_alter_new", false);
        if(null != table){
            service.ddl().drop(table);
        }
        //修改表
        //如果数据库支持 合并成尽量少的DDL
        table = new Table("tab_alter");
        table.addColumn("ID","BIGINT").setAutoIncrement(true).setPrimary(true).setComment("主键");
        table.addColumn("CODE", "varchar(10)").setComment("编号");
        table.addColumn("CODE", "varchar(10)").setComment("名称");
        service.ddl().create(table);
        table = service.metadata().table("tab_alter");
        //修改表名
        table.setNewName("tab_alter_new");
        service.ddl().save(table);
        //修改备注
        table = service.metadata().table("tab_alter_new");
        table.setComment("新备注:"+DateUtil.format());
        service.ddl().save(table);
        //添加列
        Column column = new Column();
        column.setName("new_id");
        column.setType("int");
        column.setTable(table);
        service.ddl().save(column);
        //修改列
        column = new Column();
        column.setName("new_id");
        column.setType("int");
        column.setTable(table);
        column.setNewName("new_id_alter");
        service.ddl().save(column);
        //修改主键
        //修改列长度
        //修改数据类型
        //修改列备注
        //添加自增
        //取消自增
        //添加主键
        //删除主键
        //删除主键列
        //添加新列并主键
        service.ddl().save(table);



    }
    @Test
    public void change() throws Exception{
        Table table = service.metadata().table("t_change");
        if(null != table){
            service.ddl().drop(table);
        }
        table = new Table("t_change");
        table.addColumn("ID","INT").setPrimary(true).setAutoIncrement(true);
        table.addColumn("code","number(10,2)");
        service.ddl().create(table);
        DataRow row = new DataRow();
        row.put("code", "12345678.12");
       // service.insert(table, row);

        Column column = table.getColumn("code");
        column.setType("number(5,2)");
        service.ddl().alter(table);
    }

    @Test
    public void length() throws Exception{
        //修改表名
        Table table = service.metadata().table("c_test");
        if(null != table){
            log.warn("删除表:"+table.getName());
            service.ddl().drop(table);
        }
        log.warn("创建表");
        table = new Table();
        table.setName("c_test");
        table.setComment("表备注");
        table.addColumn("ID", "int").primary(true).autoIncrement(true).setComment("主键说明");
        Column name = new Column();
        name.setType("varchar2");
        name.setName("USER_NAME");
        name.setPrecision(100);
        table.addColumn(name);
        service.ddl().create(table);


    }
    @Test
    public void doris(){
        ConfigTable.IS_LOG_ADAPTER_MATCH = true;
        ServiceProxy.service("doris").tables();
    }
    @Test
    public void alters() throws Exception{
        Table table = service.metadata().table( "sd_order_pk");
        table.setNewName("t_new_name511111");

        // 增加phone列
        Column phoneColumn = new Column();
        phoneColumn
                .setName("phone111")
                .setType("varchar(10)")
                .setNullable(false)
                .setPrimaryKey(false)
                .setComment("电话1")
                .setDefaultValue(null);
        table.addColumn(phoneColumn);
//         修改name列的长度和备注
        Column nameColumn = new Column();
        nameColumn
                .setName("newName")
                .setNewName("newName1")
                .setType("varchar")
                .setPrecision(30)
                .setComment("姓名备注修改后");
        table.addColumn(nameColumn);
        service.ddl().alter(table);
    }
    @Test
    public void adapter() throws Exception {
        MySQLAdapter mySQLAdapter = new MySQLAdapter();
        Table diffTable = new Table();
        List<Run> runs = mySQLAdapter.buildAlterRun(null, diffTable,diffTable.columns());
    }
    @Test
    public void test() throws Exception{
        Table table = service.metadata().table("crm_user");
        service.ddl().save(table);
    }
    @Test
    public void nullable() throws Exception{
        Table table = service.metadata().table("crm_user");
        table.getColumn("NAME").setNullable(false);
        service.ddl().save(table);
    }

    /**
     * 初始货表
     * @param name 表名
     * @return Table
     */
    public Table init(String name) throws Exception{
        //查询表结构
        Table table = service.metadata().table(name, false); //false表示不加载表结构，只简单查询表名
        //如果已存在 删除重键
        if(null != table){
            service.ddl().drop(table);
        }
        table = new Table(name);
        //根据不同数据库长度精度有可能忽略
        table.addColumn("ID", "bigint",12,11).primary(true).autoIncrement(true).setComment("主键");
        table.addColumn("CODE", "varchar(20)").setComment("编号");
        table.addColumn("DEFAULT_NAME", "varchar(50)").setComment("名称").setDefaultValue("A");
        table.addColumn("NAME", "varchar(50)").setComment("名称");
        table.addColumn("O_NAME", "varchar(50)").setComment("原列表");
        table.addColumn("SALARY", "decimal(10,2)").setComment("精度").setNullable(false);
        table.addColumn("SALARY_12", "decimal(10,2)").setComment("精度").setNullable(false);
        table.addColumn("TINT", "tinyint").setComment("mysql2oracle number");
        table.addColumn("DEL_COL", "varchar(50)").setComment("删除");
        table.addColumn("UPDATE_TIME", "timestamp").setOnUpdate("CURRENT_TIMESTAMP");
        table.addColumn("CREATE_TIME", "datetime").setComment("创建时间").setDefaultValue(JDBCAdapter.SQL_BUILD_IN_VALUE.CURRENT_DATETIME);
        service.ddl().create(table);
        //获取执行的SQL
        List<String> ddls = table.ddls();
        for(String ddl:ddls){
            System.out.println(ddl);
        }
        table = service.metadata().table(name);
        Index index = new Index();
        index.addColumn("SALARY");
        index.addColumn("CODE");
        index.setName("IDX_SALARY_CODE");
        index.setUnique(true);
        index.setTable(table);
        service.ddl().add(index);


        return table;
    }

    /**
     * 修改列类型
     * @throws Exception
     */
    @Test
    public  void all() throws Exception{
        String tableName = "c_test";
        String updateName = "b_test";
        String updateComment = "新comment";
        Table table = init(tableName);
        //修改表名称，修改表注释
        //修改列名称，修改列注释，修改列属性，修改列长度，修改列是否允许为空，修改列主键，修改列默认值，添加列，删除列
        //注意部分数据库不能修改索引中的列
        //修索引名称，修改索引注释，修改索引类型，修改索引方法，新增表索引，删除表索引
        /************************************************************************************************************
         *
         *                         rename操作会造成很大的疑惑 请参考 http://doc.anyline.org/aa/e1_3601
         *
         ************************************************************************************************************/

        /*
         * 1.修改表名
         * 修改名称比较特殊，因为需要同时保留新旧名称，否则就不知道要修改哪个表或列了
         * 同时要注意改名不会检测新名称是否存在 所以改名前要确保 新名称 没有被占用
         */
        Table chk = service.metadata().table(updateName, false);
        if(null != chk){
            service.ddl().drop(chk);
        }
        table.update().setName(updateName).setComment(updateComment);
        /*
         * 2.修改属性，注释，非空 如果不存在则创建
         */
        Column col = table.getColumn("NAME");
        if(null == col){
            col = new Column("NAME");
        }
        col.setType("int").setDefaultValue("123").setComment("新类型").setNullable(false);

        Column def = table.getColumn("DEFAULT_NAME");
        def.setDefaultValue("ABC");
        /*
         * 3.删除列
         */
        col = table.getColumn("DEL_COL");
        col.drop();
        /*
         * 4.修改列名
         */
        col = table.getColumn("O_NAME");
        col.update().setName("N_NAME").setComment("新列名");
        /*
         * 5.修改精度
         */
        col = table.getColumn("SALARY_12");
        col.setPrecision(18);
        col.setScale(9);
        col.setNullable(true);
        /*
         * 6.换主键
         */
        table.addColumn("PK_CODE", "INT");
        table.setPrimaryKey("PK_CODE");

        service.ddl().save(table);

        table = service.metadata().table(updateName);
        /*
         * 7.删除主键之外的索引
         */
        LinkedHashMap<String,Index> indexes = table.getIndexes();
        for(Index index:indexes.values()){
            if(!index.isPrimary()){
                service.ddl().drop(index);
            }
        }
        /*
         * 8.添加新索引
         */
        Index index = new Index();
        index.addColumn("ID");
        index.addColumn("SALARY");
        index.setName("IDX_ID_SALARY");
        index.setUnique(true);
        index.setTable(table);
        service.ddl().add(index);

        chk = service.metadata().table(tableName);
        //原表名不存在
        Assertions.assertNull(chk);

        //新表名存在
        table = service.metadata().table(updateName);
        Assertions.assertNotNull(table);

        //新表注释
        Assertions.assertEquals(table.getComment(), updateComment);

        //已删除列不存在
        col = table.getColumn("DEL_COL");
        Assertions.assertNull(col);

        //改名列不存在
        col = table.getColumn("O_NAME");
        Assertions.assertNull(col);

        //改名新列存在
        col = table.getColumn("N_NAME");
        Assertions.assertNotNull(col);

        //列注释
        Assertions.assertEquals("新列名", col.getComment());

        //修改类型
        col = table.getColumn("NAME");
        //varchar varchar2
        //Assertions.assertEquals("VARCHAR(100)", col.getFullType().toUpperCase());

        //默认值
        Assertions.assertEquals("123", col.getDefaultValue());

        //null > not null
        Assertions.assertEquals(0, col.isNullable());

        //新主键
        Assertions.assertEquals(-1, col.isPrimaryKey());

        //原主键取消
        col = table.getColumn("ID");
        //("1", col.isPrimaryKey()+"");

        //原主键自增取消
        //Assertions.assertNotEquals("1", col.isAutoIncrement()+"");

        //列长度精度
        col = table.getColumn("SALARY_12");
        Assertions.assertEquals(18, col.getPrecision());
        Assertions.assertEquals(9, col.getScale());

        //not null > null
        Assertions.assertEquals(1, col.isNullable());

        //原索引删除
        Index idx = table.getIndex("IDX_SALARY_CODE");
        Assertions.assertNull(idx);
        //新索引
        idx = table.getIndex("IDX_ID_SALARY");
        Assertions.assertNotNull(idx);
        //唯一索引
        Assertions.assertTrue(idx.isUnique());




        LinkedHashMap<String,Column> columns = service.metadata().table("b_test").getColumns();
        for(Column column:columns.values()){
            System.out.println("\ncolumn:"+column.getName());
            System.out.println("type:"+column.getFullType());
        }

    }
    //生成SQL不执行
    @Test
    public void sql() throws Exception{
        Table table = new Table("TAB_A");
        table.addColumn("ID", "int").autoIncrement(true).primary(true);
        table.addColumn("CODE", "VARCHAR(10)").setDefaultValue("ABC");
        //如果确定数据库类型直接创建adapter实例
        DriverAdapter adapter = new OracleAdapter();
        List<Run> sqls = adapter.buildCreateRun(RuntimeHolder.runtime(),table);
        for(Run sql:sqls){
            System.out.println(sql.getFinalUpdate());
        }

        //如果在运行时有多个数据库可以通过SQLAdapterUtil辅助确定数据库类型
        //为什么需要两个参数，getAdapter调用非常频繁,解析过程中需要与数据库建立连接比较耗时，第一个参数是用于缓存，第一次成功解析数据库类型后会缓存起来，后续不再解析
        DataRuntime runtime = RuntimeHolder.runtime();
        adapter = runtime.getAdapter();

        sqls = adapter.buildCreateRun(RuntimeHolder.runtime(),table);
        for(Run sql:sqls){
            System.out.println(sql.getFinalUpdate());
        }

    }
    public void trigger() throws Exception{
        Table tb = service.metadata().table("TAB_USER", false);
        if(null != tb){
            service.ddl().drop(tb);
        }
        tb = new Table("TAB_USER");
        tb.addColumn("ID","INT").autoIncrement(true).primary(true);
        tb.addColumn("CODE", "varchar(10)");
        service.ddl().create(tb);

        Trigger trigger = new Trigger();
        trigger.setName("TR_USER");
        trigger.setTime(org.anyline.metadata.Trigger.TIME.AFTER);
        trigger.addEvent(org.anyline.metadata.Trigger.EVENT.INSERT);
        trigger.setTable("TAB_USER");
        trigger.setDefinition("UPDATE aa SET code = 1 WHERE id = NEW.id;");
        service.ddl().create(trigger);

        trigger = service.metadata().trigger("TR_USER");
        if(null != trigger){
            System.out.println("TRIGGER TABLE:"+trigger.getTableName(true));
            System.out.println("TRIGGER NAME:"+trigger.getName());
            System.out.println("TRIGGER TIME:"+trigger.getTime());
            System.out.println("TRIGGER EVENT:"+trigger.getEvents());
            System.out.println("TRIGGER define:"+trigger.getDefinition());
            service.ddl().drop(trigger);
        }
    }
    //外键
    public void foreign() throws Exception{

        Table tb = service.metadata().table("TAB_B");
        if(null != tb){
            service.ddl().drop(tb);
        }
        Table ta = service.metadata().table("TAB_A");
        if(null != ta){
            service.ddl().drop(ta);
        }
        //创建组合主键
        ta = new Table("TAB_A");
        ta.addColumn("ID", "int").setNullable(false).primary(true);
        ta.addColumn("CODE", "varchar(10)").setNullable(false).primary(true);
        ta.addColumn("NAME", "varchar(10)");
        service.ddl().create(ta);


        tb = new Table("TAB_B");
        tb.addColumn("ID", "int").primary(true).autoIncrement(true);
        tb.addColumn("AID", "int");
        tb.addColumn("ACODE", "varchar(10)");
        service.ddl().create(tb);
        //创建组合外键
        ForeignKey foreign = new ForeignKey("fkb_id_code");
        foreign.setTable("TAB_B");
        foreign.setReference("TAB_A");
        foreign.addColumn("AID","ID");
        foreign.addColumn("ACODE","CODE");
        service.ddl().add(foreign);

        //查询组合外键
        LinkedHashMap<String, ForeignKey> foreigns = service.metadata().foreigns("TAB_B");
        for(ForeignKey item:foreigns.values()){
            System.out.println("外键:"+item.getName());
            System.out.println("表:"+item.getTableName(true));
            System.out.println("依赖表:"+item.getReference().getName());
            LinkedHashMap<String, Column> columns = item.getColumns();
            for(Column column:columns.values()){
                System.out.println("列:"+column.getName()+"("+column.getReference()+")");
            }
        }
        //根据列查询外键
        foreign = service.metadata().foreign("TAB_B", "AID","ACODE");
        if(null != foreign) {
            System.out.println("外键:" + foreign.getName());
            System.out.println("表:" + foreign.getTableName(true));
            System.out.println("依赖表:" + foreign.getReference().getName());
            LinkedHashMap<String, Column> columns = foreign.getColumns();
            for (Column column : columns.values()) {
                System.out.println("列:" + column.getName() + "(" + column.getReference() + ")");
            }
        }
    }
    @Test
    public void view() throws Exception {
        View view = new View("V_CRM_USER");
        view.setDefinition("SELECT * FROM CRM_USER WHERE ID > 0");
        service.ddl().create(view);
    }
    public void type() throws Exception{
        Table table = service.metadata().table("c_test");
        if(null != table){
            service.ddl().drop(table);
        }
        table = new Table("c_test");
        table.setComment("表备注");
        table.addColumn("ID", "INT").autoIncrement(true).primary(true);
        table.addColumn("REG_TIME", "TIME");
        service.ddl().save(table);
        try {
            DataRow row = new DataRow();
            row.put("REG_TIME", LocalTime.now());
            service.insert("c_test", row);
        }catch (Exception e){
            //有个id不能为空的跳过
        }
    }
    @Test
    public void charset() throws Exception{
        Table table = service.metadata().table("table_char", false);
        if(null != table){
            service.ddl().drop(table);
        }
        table = new Table("table_char");
        table.setComment("表备注");
        table.addColumn("ID", "BIGINT").setAutoIncrement(true).setPrimary(true).setComment("主键");
        table.addColumn("CODE","varchar(10)").setCharset("utf8mb4").setCollate("utf8mb4_0900_ai_ci").setComment("编号");
        table.addColumn("NAME", "varchar(10,2)");
        table.setCharset("utf8mb4");
        table.setCollate("utf8mb4_0900_ai_ci");

        service.ddl().create(table);
    }
    @Test
    public void queryTable(){
        ConfigTable.IS_AUTO_CHECK_METADATA = true;/*
        DataSet set = service.querys("simple.CRM_USER");
        LinkedHashMap<String,Column> columns = set.getMetadatas();
        System.out.println(columns);*/
        DataRow row = new DataRow();
        row.put("CODE","1");
       /* while (true) {
            try {
                service.save("simple.CRM_USER", row);
            }catch (Exception e){
                e.printStackTrace();
            }
        }*/
    }
    @Test
    public void table() throws Exception{
        System.out.println("\n-------------------------------- start table  --------------------------------------------\n");

        LinkedHashMap<String,Table> tables = service.metadata().tables();
        log.warn("检索表数量:"+tables.size());
        for(Table table:tables.values()){
            log.warn("表:"+table.getName());
        }
        //修改表名
        Table table = service.metadata().table("c_test");
        if(null != table){
            log.warn("删除表:"+table.getName());
            service.ddl().drop(table);
        }
        log.warn("创建表");
        table = new Table();
        table.setName("c_test");
        table.setComment("表备注");
        table.addColumn("ID", "int").primary(true).autoIncrement(true).setComment("主键说明");

        table.addColumn("NAME","varchar(50)").setComment("名称");
        table.addColumn("A_CHAR","varchar(50)");
        table.addColumn("reg_time", "datetime").setComment("注册时间");
        Column timeColumn = new Column();
        timeColumn.setName("time");
        timeColumn.setType("timestamp");
        timeColumn.setNullable(true);
        timeColumn.setPrimaryKey(false);
        timeColumn.setComment("时间");
        timeColumn.setDefaultValue(null);
        table.addColumn(timeColumn);
        service.ddl().save(table);
        ConfigTable.IS_DDL_AUTO_DROP_COLUMN = true;
        table = service.metadata().table("c_test");
        table.setName("c_test");
        table.setComment("新备注");
        table.addColumn("a_char","varchar(50)");
        table.addColumn("NAME_"+ BasicUtil.getRandomNumberString(5),"varchar(50)").setComment("添加新列");
        service.ddl().save(table);


        table = service.metadata().table("c_test");
        if(null != table) {
            log.warn("查询表结构:" + table.getName());
            LinkedHashMap<String, Column> columns = table.getColumns();
            for (Column column : columns.values()) {
                log.warn("列:" + column.toString());
            }
        }

        //修改表结构两类场景
        //1.在原表结构上添加修改列
        if(null != table) {
            table.getColumn("NAME").setComment("新备注名称");
            table.addColumn("NAMES", "varchar(50)").setComment("名称S");
            service.ddl().save(table);
        }

        //2.构造表结构(如根据解析实体类的结果) 与数据库对比
        /* ***************************************************************************************************************
         * 		这里务必注意:因为update是新构造的(而不像1中是从数据库中查出来的会包含数据库中所有的列)所以会存在update中不存在而数据库中存在的列
         * 		对于这部分列有两种处理方式:1)从数据库中删除 2)忽略
         * 		默认情况下会忽略,如果要删除可以设置table.setAutoDropColumn(true)
         *
         * ****************************************************************************************************************/

        //如果数据库中已存在同名的表 则执行更新
        Table update = new Table("c_test");//尽量不要new Table  因为表结构上有许多许多的配置项
        update.setAutoDropColumn(true); //删除update中不存在的列
        update.addColumn("ID", "int").primary(true).autoIncrement(true).setComment("主键说明");
        update.addColumn("CODE","varchar(50)").setComment("编号");
        //这里表原来少了NAME  NAMES  A_CHAR 三列、执行save时会删除这三列
        service.ddl().save(update);
        table = service.metadata().table("TEST_PK");
        if(null != table){
            service.ddl().drop(table);
        }

        table = new Table("TEST_PK");
        table.addColumn("NAME", "varchar(10)");
        service.ddl().save(table);

        table.addColumn("ID", "int").primary(true).autoIncrement(true);
        service.ddl().save(table);


        table = service.metadata().table("TEST_PK");
        Column pcol = table.addColumn("PKID", "int");
        PrimaryKey pk = new PrimaryKey();
        pk.addColumn(pcol);
        //修改主键
        table.setPrimaryKey(pk);
        service.ddl().save(table);

        //删除主键
        table = service.metadata().table("TEST_PK");
        table.getColumn("ID").setPrimary(false);
        service.save(table);

        //修改表名
        table.update().setName("test_pk_"+ DateUtil.format("yyyyMMddHHmmss"));
        service.ddl().save(table);

        System.out.println("\n-------------------------------- end table  ----------------------------------------------\n");
    }

    @Test
    public void drop_pk() throws Exception {
        Table table = service.metadata().table("c_test", false);
        if (null != table) {
            service.ddl().drop(table);
        }
        //在列属性上设置主键
        table = new Table("c_test");
        table.addColumn("ID", "INT").setPrimary(true);
        table.addColumn("CODE", "VARCHAR(20)").setPrimary(true);
        table.addColumn("NAME", "VARCHAR(20)");
        service.ddl().create(table);


        PrimaryKey pk = new PrimaryKey();
        pk.setTable("c_test");

        //直接删除主键
        //service.ddl().drop(pk);

        //alter table过滤 删除主键
        pk.drop();
        table.setPrimaryKey(pk);
        service.ddl().save(table);
    }
        @Test
    public void pk() throws Exception{
        Table table = service.metadata().table("c_test", false);
        if(null != table){
            service.ddl().drop(table);
        }
        //在列属性上设置主键
        table = new Table("c_test");
        table.addColumn("ID", "INT").setPrimary(true);
        table.addColumn("CODE","VARCHAR(20)").setPrimary(true);
        table.addColumn("NAME","VARCHAR(20)");
        service.ddl().create(table);
        PrimaryKey pk = new PrimaryKey();
        pk.setTable("c_test");
        service.ddl().drop(pk);


        //在表上单独设置主键
        service.ddl().drop(table);
        table = new Table("c_test");
        table.addColumn("ID", "INT");
        table.addColumn("CODE","VARCHAR(20)");
        table.addColumn("NAME","VARCHAR(20)");
        table.setPrimaryKey("ID","CODE");
        service.ddl().create(table);

        table = service.metadata().table("c_test");
        pk = table.getPrimaryKey();
        service.ddl().drop(pk);


        //创建主键对象
        service.ddl().drop(table);
        table = new Table("c_test");
        table.addColumn("ID", "INT");
        table.addColumn("CODE","VARCHAR(20)");
        table.addColumn("NAME","VARCHAR(20)");
         pk = new PrimaryKey();
        pk.addColumn("ID").addColumn("CODE");
        table.setPrimaryKey(pk);
        service.ddl().create(table);

        table = service.metadata().table("c_test");
        table.getPrimaryKey().delete();
        service.ddl().create(table);

        pk = new PrimaryKey();
        pk.addColumn("ID").addColumn("NAME").setName("pk");
        table.setPrimaryKey(pk);
        service.ddl().save(table);

    }
    @Test
    public void column() throws Exception{
        System.out.println("\n-------------------------------- start column  -------------------------------------------\n");
        service = ServiceProxy.service("oracle");
        Table table = service.metadata().table("c_test");
        if(null != table){
            service.ddl().drop(table);
        }

        table = new Table();
        table.setName("c_test");
        table.setComment("表备注");
        table.addColumn("ID", "int").primary(true).autoIncrement(true).setComment("主键说明");

        table.addColumn("NAME","varchar(50)").setComment("名称");
        table.addColumn("A_CHAR","varchar(50)");
        table.addColumn("DEL_COL","varchar(50)");
        LinkedHashMap<String, Column> columns = table.getColumns();
        columns.put("NEW_CHAR", new Column().setName("NEW_CHAR").setType("int"));
        service.ddl().save(table);

        table = service.metadata().table("c_test");

        //添加列
        String tmp = "NEW_"+BasicUtil.getRandomNumberString(3);
        table.addColumn(tmp, "int");
        service.ddl().save(table);
        //删除列
        Column dcol = table.getColumn(tmp);
        dcol.delete();
        service.ddl().save(table);

        //修改列属性
        Column column = table.getColumn("NAME");
        column.setTypeName("int");	//没有数据的情况下修改数据类型
        column.setPrecision(0);
        column.setScale(0);
        column.setDefaultValue("1");
        column.setNullable(false);
        boolean result = service.save(column);

        column = new Column();
        column.setTable("c_test");
        column.setName("A_CHAR");
        column.setTypeName("int");	//没有数据的情况下修改数据类型
        column.setPrecision(0);
        column.setScale(0);
        column.setDefaultValue("1");
        column.setNullable(false);

        service.ddl().save(column);
        column.setTable("c_test");
        column.setName("A_CHAR");
        //修改列
        //没有值的属性 默认同步原有的数据库结构
        //如果不修改列名，直接修改column属性
        column.setTypeName("varchar(10)");
        column.setComment("测试备注1"+ DateUtil.format());
        column.setDefaultValue("2");
        log.warn("修改列");
        service.ddl().save(column);

        //修改列名2种方式
        //注意:修改列名时，不要直接设置name属性,修改数据类型时，不要直接设置typeName属性,因为需要原属性
        // 1.可以设置newName属性(注意setNewName返回的是update)
        column.setNewName("B_TEST").setTypeName("varchar(20)");
        log.warn("修改列名");
        service.ddl().save(column);



        // 2.可以在update基础上修改
        //如果设置了update, 后续所有更新应该在update上执行
        column.update().setName("C_TEST").setPosition(0).setTypeName("VARCHAR(20)");
        log.warn("修改列名");
        service.ddl().save(column);

        column = new Column();
        column.setName("c_test").setNewName("d_test");
        column.setTypeName("varchar(1)");
        column.setTable("c_test");
        service.ddl().save(column);
/*
		column = new Column("id");
		column.setTable("t");
		column.setCatalog("c");
		column.setSchema("s");

		log.warn("删除列");
		service.ddl().drop(column);*/

        Table tab = new Table("c_test");
        tab.addColumn("ID", "int");
        service.ddl().save(tab);
        //添加修改自增(没有实现)

        System.out.println("\n-------------------------------- end column  ---------------------------------------------\n");

    }
    @Test
    public void pks() throws Exception{
        service = ServiceProxy.service("oracle");
        Table table = service.metadata().table("CRM_USER");
        if(null != table){
            service.ddl().drop(table);
        }
        table = new Table("CRM_USER");

        table.addColumn("NAME","varchar(50)").setComment("名称");
        table.addColumn("A_CHAR","varchar(50)").setPrimary(true);
        table.addColumn("DEL_COL","varchar(50)").setPrimary(true);
        service.ddl().create(table);
        service.ddl().drop(table);

        table.addColumn("NAME","varchar(50)").setComment("名称");
        table.addColumn("A_CHAR","varchar(50)");
        table.addColumn("DEL_COL","varchar(50)");
        PrimaryKey pk = new PrimaryKey();
        pk.addColumn("A_CHAR").addColumn("DEL_COL");
        table.setPrimaryKey(pk);
        service.ddl().create(table);
        service.ddl().drop(table);

        table.addColumn("NAME","varchar(50)").setComment("名称");
        table.addColumn("A_CHAR","varchar(50)");
        table.addColumn("DEL_COL","varchar(50)");
        table.setPrimaryKey("A_CHAR", "DEL_COL");
        service.ddl().create(table);
    }
    public void exception() throws Exception{

        String tab = "A_EXCEPTION";
        String col = "A_CHAR2INT";
        System.out.println("\n-------------------------------- start exception  ----------------------------------------\n");
        ConfigTable.AFTER_ALTER_COLUMN_EXCEPTION_ACTION				= 1000			;   // DDL修改列异常后 0:中断修改 1:删除列 n:总行数小于多少时更新值否则触发另一个监听
        // 0:中断执行
        // 1:直接修正
        // n:行数<n时执行修正  >n时触发另一个监听(默认返回false)
        Table table = service.metadata().table(tab);
        if(null != table){
            service.ddl().drop(table);
        }
        table = new Table(tab);
        table.addColumn("ID", "INT").primary(true).autoIncrement(true);
        table.addColumn(col, "VARCHAR(20)");
        service.ddl().create(table);

        table.addColumn("CODE", "INT");
        service.ddl().save(table);
        table = service.metadata().table(tab);
        table.addColumn("CODE", "INT");
        service.ddl().save(table);
        //表中有数据的情况下
        DataRow row = new DataRow();
        //自增列有可能引起异常
        try {
            row.put(col, "123A");
            service.save(tab, row);
        }catch (Exception e){
            log.error(e.getMessage());
        }
        log.warn("表中有数据的情况下修改列数据类型");
        Column column = new Column();
        column.setTable(tab);
        column.setName(col);
        //这一列原来是String类型 现在改成int类型
        //如果值不能成功实现殷式转换(如123A转换成int)会触发一次默认的DDListener.afterAlterException
        //如果afterAlterException返回true，会再执行一次alter column如果还失败就会抛出异常
        //如果不用默认listener可以column.setListener
        column.setTypeName("int");
        column.setPrecision(0);
        service.ddl().save(column);
        System.out.println("\n-------------------------------- end  exception  -----------------------------------------\n");
    }
    @Test
    public void index() throws Exception{
        System.out.println("\n-------------------------------- start index  --------------------------------------------\n");

        Table tab = service.metadata().table("B_TEST", false);
        if(null != tab){
            service.ddl().drop(tab);
        }
        tab = new Table("B_TEST");
        tab.addColumn("ID", "INT").autoIncrement(true).primary(true);
        tab.addColumn("CODE", "INT");
        service.ddl().save(tab);

        LinkedHashMap<String, Index> indexes = service.metadata().indexes("crm_user");
        if(null != indexes) {
            for (Index item : indexes.values()) {
                System.out.println("所引:" + item.getName());
                System.out.println("是否主键:" + item.isPrimary());
                System.out.println("是否物理所引:" + item.isCluster());
                System.out.println("是否唯一:" + item.isUnique());
                LinkedHashMap<String, Column> columns = item.getColumns();
                for (Column column : columns.values()) {
                    System.out.println("包含列:" + column.getName());
                }
                //如果删除自增长主键 有可能会抛出异常： there can be only one auto column and it must be defined as a key

                System.out.println("删除索引:" + item.getName());
                try {
                    if (!item.isPrimary()) {
                        service.ddl().drop(item);
                    }
                } catch (Exception e) {
                    log.error(e.getMessage());
                }
            }
        }

        Index index = service.metadata().index("crm_user_index_CODE");
        if(null == index){
            index = new Index();

            index.setName("crm_user_index_CODE"); //如果不指定名称，会生成一个随机名称，如果指定了名称但与现有索引重名 会抛出异常
            index.setUnique(true);
            index.setTable(tab);
            index.addColumn(new Column("CODE"));
            service.ddl().add(index);
        }

        //查询指定表上的全部索引
        indexes = service.metadata().indexes("c_test");
        for(Index idx: indexes.values()){
            System.out.println("\n索引表:"+idx.getTable(false));
            System.out.println("索引名称:"+idx.getName());
            Map<String,Column> columns = idx.getColumns();
            for(String col:columns.keySet()){
                System.out.println("column:"+col);
            }
        }
        //查询全部索引(当前库)
        indexes = service.metadata().indexes();
        for(Index idx: indexes.values()){
            System.out.println("\n索引表:"+idx.getTable(false));
            System.out.println("索引名称:"+idx.getName());
            Map<String,Column> columns = idx.getColumns();
            for(String col:columns.keySet()){
                System.out.println("column:"+col);
            }
        }
        //查询全部索引(包含其他有权限的库)
        List<Index> list= service.metadata().indexes(true);
        for(Index idx: list){
            System.out.println("\n索引表:"+idx.getTable(false));
            System.out.println("索引名称:"+idx.getName());
            Map<String,Column> columns = idx.getColumns();
            for(String col:columns.keySet()){
                System.out.println("column:"+col);
            }
        }

        System.out.println("\n-------------------------------- end index  ----------------------------------------------\n");
    }
    @Test
    public void pk2() throws Exception{
        Table tab = service.metadata().table("B_TEST", false);
        if(null != tab){
            service.ddl().drop(tab);
        }
        tab = new Table("B_TEST");
        tab.addColumn("ID", "INT").autoIncrement(true).primary(true);
        tab.addColumn("CODE", "INT");
        service.ddl().save(tab);

        tab = new Table("B_TEST");
        tab.addColumn("ID", "INT").primary(false);
        tab.addColumn("CODE", "INT");
        tab.addColumn("IDS", "INT").primary(true).autoIncrement(true).setComment("新主键");
        tab.execute(false);
        service.ddl().save(tab);
        List<String> ddls = tab.ddls();
        for(String ddl:ddls){
            System.out.println(ddl);
        }
        List<Run> runs = tab.runs();
        for(Run run:runs){
            System.out.println(run.getFinalUpdate());
        }
    }
    @Test
    void testUpdateConfigStore() throws Exception {

        String tableName = "test6661";

        // 创建测试表
        Table table = service.metadata().table(tableName, false);
        if(null != table){
            service.ddl().drop(table);
        }
        table = new Table<>(tableName).setComment("测试表");
        table.addColumn("tid", "int").setPrimary(true).setAutoIncrement(true);
        table.addColumn("name", "varchar(50)");
        service.ddl().save(table);

        // 写入数据
        List<HashMap<String, Object>> insertRows = new ArrayList<>();

        HashMap<String, Object> row1 = new HashMap<>();
        row1.put("tid", 1);
        row1.put("name", "t1");

        HashMap<String, Object> row2 = new HashMap<>();
        row1.put("tid", 2);
        row1.put("name", "t2");

        insertRows.add(row1);
        insertRows.add(row2);

        service.insert(100, tableName, insertRows);

        ConfigStore configStore = new DefaultConfigStore();
        configStore.setPrimaryKey("tid");
        DataSet set = new DataSet();
        DataRow row = set.add();
        row.put("tid", 2);
        row.put("name", "n");

        service.update(tableName, set, configStore);

        // 更新数据

        List<HashMap<String, Object>> updateRows = new ArrayList<>();

        HashMap<String, Object> row3 = new HashMap<>();

        HashMap<String, Object> row4 = new HashMap<>();
        row4.put("tid", 2);
        row4.put("name", "up-tttt");

        updateRows.add(row3);
        updateRows.add(row4);


        service.update(tableName, updateRows, configStore);

    }
    public void clear(){
        System.out.println("\n=============================== START clear =========================================\n");
        try {
            service.ddl().drop(new Table("c_test"));
            service.ddl().drop(new Table("b_test"));
            service.ddl().drop(new Table("c_test"));
            service.ddl().drop(new Table("i_test"));
        }catch (Exception e){}
        System.out.println("\n=============================== START clear =========================================\n");
    }
    @Test public void init() throws Exception{
        //先创建个表
        Table table = service.metadata().table("crm_user");
        if(null != table){
            service.ddl().drop(table);
        }
        table = new Table("crm_user");
        Column column = new Column("ID").autoIncrement(true).setType("int(20)").primary(true);
        table.addColumn(column);
        table.addColumn("CODE","varchar").setLength(255);
        table.addColumn("NAME","varchar(10)");
        Index index = new Index<>().setName("CODE_INDEX").addColumn("CODE").setCluster(true);
        index.setTable(table);
        table.add(index);
        service.ddl().create(table);
    }
}
