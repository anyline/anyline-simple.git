package org.anyline.simple.nebula;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Arrays;

@SpringBootApplication
public class NebulaApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(NebulaApplication.class);
        ConfigurableApplicationContext context = application.run(args);
    }
}
