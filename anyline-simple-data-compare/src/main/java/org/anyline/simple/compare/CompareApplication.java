package org.anyline.simple.compare;

import org.anyline.data.run.Run;
import org.anyline.metadata.Column;
import org.anyline.metadata.Table;
import org.anyline.metadata.differ.ColumnsDiffer;
import org.anyline.metadata.differ.MetadataDiffer;
import org.anyline.metadata.differ.TableDiffer;
import org.anyline.metadata.differ.TablesDiffer;
import org.anyline.proxy.ServiceProxy;
import org.anyline.util.ConfigTable;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.LinkedHashMap;
import java.util.List;

@SpringBootApplication
public class CompareApplication {


	public static void main(String[] args){
		SpringApplication application = new SpringApplication(CompareApplication.class);
		application.run(args);

		table();
	}
	public static void table(){
		Table a = ServiceProxy.metadata().table("project_info");
		Table b = ServiceProxy.metadata().table("project_info_temp");
		TableDiffer differ = a.compare(b, MetadataDiffer.DIRECT.ORIGIN);
		List<Run> runs = ServiceProxy.ddl(differ);
		for(Run run:runs){
			System.out.println(run.getFinalExecute()+";\n");
		}
	}
	public static  void tables(){

		ConfigTable.IS_PRINT_EXCEPTION_STACK_TRACE = true;
		LinkedHashMap<String, Table> as = ServiceProxy.metadata().tables(1, true);
		LinkedHashMap<String, Table> bs = ServiceProxy.service("pg").metadata().tables(1, true);
		//对比过程 默认忽略catalog, schema
		TablesDiffer differ = TablesDiffer.compare(as, bs);

		System.out.println("===================================== DDL ================================================");
		//设置生成的SQL在源库还是目标库上执行
		differ.setDirect(MetadataDiffer.DIRECT.ORIGIN);
		List<Run> runs = ServiceProxy.ddl(differ);
		for(Run run:runs){
			System.out.println(run.getFinalExecute()+";\n");
		}

		//以下在实际应用中不需要，只是为了说明详细过程
		LinkedHashMap<String, Table> adds =  differ.getAdds();
		System.out.println("原表"+as);
		System.out.println("表表"+bs);
		//由a > b
		System.out.println("++++++++++++++++++++++++++++++++++++++++++添加表++++++++++++++++++++++++++++++++++++++");
		for(Table item:adds.values()){
			System.out.println(item);
		}
		LinkedHashMap<String, Table> alters = differ.getAlters();
		System.out.println("///////////////////////////////////////////修改表/////////////////////////////////////");
		for(Table item:alters.values()){
			System.out.println(item);
		}

		LinkedHashMap<String, TableDiffer> differs = differ.getDiffers();
		for(TableDiffer dif:differs.values()){
			System.out.println("修改表:"+dif.getOrigin() +" > "+dif.getDest());
			ColumnsDiffer columnsDiffer = dif.getColumnsDiffer();
			for(Column column:columnsDiffer.getAdds().values()){
				System.out.println("+添加列:"+column);
			}
			for(Column column:columnsDiffer.getAlters().values()){
				System.out.println("/修改列:"+column+" > "+column.getUpdate());
			}
			for(Column column:columnsDiffer.getDrops().values()){
				System.out.println("-删除列:"+column);
			}

		}
		LinkedHashMap<String, Table> drops = differ.getDrops();
		System.out.println("---------------------------------------------删除表----------------------------------------");
		for(Table item:drops.values()){
			System.out.println(""+item);
		}
	}
}
