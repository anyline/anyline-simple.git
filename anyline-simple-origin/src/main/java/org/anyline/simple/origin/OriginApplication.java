package org.anyline.simple.origin;

import org.anyline.adapter.EnvironmentWorker;
import org.anyline.adapter.init.DefaultEnvironmentWorker;
import org.anyline.data.transaction.TransactionState;
import org.anyline.entity.DataRow;
import org.anyline.entity.DataSet;
import org.anyline.proxy.ServiceProxy;
import org.anyline.proxy.TransactionProxy;
import org.anyline.service.AnylineService;

public class OriginApplication {
    public static void main(String[] args) throws Exception {

        EnvironmentWorker work = DefaultEnvironmentWorker.start();

        //指定数据源
        tx1();
        //默认数据源
        tx2();
    }
    public static void identity(){
        AnylineService service = ServiceProxy.service();
        System.out.println(service.query("crm_user"));

        DataSet set = new DataSet();
        for(int i=0; i<10; i++){
            DataRow row = set.add();
            row.put("CODE", i);
        }
        service.insert("crm_user", set);
        System.out.println(set);
        System.out.println(service.querys("crm_user"));
    }
    public static void tx1() throws Exception{
        AnylineService service = ServiceProxy.service("crm");
        service.truncate("crm_user");
        DataSet set = new DataSet();
        for(int i=0; i<20; i++){
            DataRow row = set.add();
            row.put("CODE", i);
            service.insert("crm_user", row);
        }
        long qty = service.count("crm_user");
        System.out.println("插入10行后 当前行数:"+qty);
        TransactionState state = TransactionProxy.start("crm");
        TransactionProxy.rollback(state);
        qty = service.count("crm_user");
        System.out.println("插入10行后 当前行数:"+qty);

        //启动事务
        state = TransactionProxy.start("crm");
        //或service.start();//因为service绑定了数据源 所以不需要数据源名称这个参数
        DataRow row = new DataRow();
        row.put("CODE", 11);
        service.insert("crm_user",row);
        qty = service.count("crm_user");
        System.out.println("插入第11行 未提交事务 当前行数:"+qty);
        TransactionProxy.commit(state);
        qty = service.count("crm_user");
        System.out.println("插入第11行 已提交事务 当前行数:"+qty);


        state = TransactionProxy.start("crm");
        row = new DataRow();
        row.put("CODE", 11);
        service.insert("crm_user",row);
        qty = service.count("crm_user");
        System.out.println("插入第12行 未回滚事务 当前行数:"+qty);
        TransactionProxy.rollback(state);
        qty = service.count("crm_user");
        System.out.println("插入第12行 已回滚事务 当前行数:"+qty);
    }
    public static void tx2() throws Exception{
        AnylineService service = ServiceProxy.service();
        service.truncate("crm_user");
        DataSet set = new DataSet();
        for(int i=0; i<10; i++){
            DataRow row = set.add();
            row.put("CODE", i);
        }
        service.insert("crm_user", set);
        long qty = service.count("crm_user");
        System.out.println("插入10行后 当前行数:"+qty);

        //启动事务
        TransactionState state = TransactionProxy.start();
        DataRow row = new DataRow();
        row.put("CODE", 11);
        service.insert("crm_user",row);
        qty = service.count("crm_user");
        System.out.println("插入第11行 未提交事务 当前行数:"+qty);
        TransactionProxy.commit(state);
        qty = service.count("crm_user");
        System.out.println("插入第11行 已提交事务 当前行数:"+qty);


        state = TransactionProxy.start();
        row = new DataRow();
        row.put("CODE", 11);
        service.insert("crm_user",row);
        qty = service.count("crm_user");
        System.out.println("插入第12行 未回滚事务 当前行数:"+qty);
        TransactionProxy.rollback(state);
        qty = service.count("crm_user");
        System.out.println("插入第12行 已回滚事务 当前行数:"+qty);
    }
}
