package org.anyline.simple.generator;

import org.anyline.entity.DataRow;
import org.anyline.entity.generator.PrimaryGenerator;
import org.anyline.metadata.Table;
import org.anyline.proxy.ServiceProxy;
import org.anyline.service.AnylineService;
import org.anyline.util.ConfigTable;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = GeneratorApplication.class)
public class GeneratorTest {

    @Test
    public void start() throws Exception{
        ConfigTable.GENERATOR.set(PrimaryGenerator.GENERATOR.SNOWFLAKE);
        AnylineService service = ServiceProxy.service("ms");
        Table table = service.metadata().table("crm_user");
        if(null != table){
            service.ddl().drop(table);
        }
        table = new Table("crm_user");
        table.addColumn("ID", "VARCHAR(64)");
        table.addColumn("NM", "VARCHAR(20)");
        table.addColumn("AGE", "INT");
        service.ddl().create(table);

        DataRow row = new DataRow();
        row.put("AGE", 10);
        service.insert("crm_user", row);
        System.out.println(row);
    }
}
