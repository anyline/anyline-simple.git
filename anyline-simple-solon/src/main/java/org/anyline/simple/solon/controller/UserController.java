package org.anyline.simple.solon.controller;

import org.anyline.annotation.Autowired;
import org.anyline.data.transaction.TransactionState;
import org.anyline.entity.DataRow;
import org.anyline.proxy.ServiceProxy;
import org.anyline.proxy.TransactionProxy;
import org.anyline.service.AnylineService;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.data.annotation.Tran;

@Controller
public class UserController {
    @Autowired
    private AnylineService service;
    @Mapping("/list")
    @Tran
    public String list() {
        DataRow row = ServiceProxy.query("crm_user");
        long count = ServiceProxy.count("crm_user");
        System.out.println(count);
        TransactionState state = null;
        try {
            state = TransactionProxy.start();
        }catch (Exception e){
            e.printStackTrace();
        }
        row = new DataRow();
        row.put("CODE", "123");
        ServiceProxy.insert("crm_user", row);
        try {
            TransactionProxy.rollback(state);
        }catch (Exception e){
            e.printStackTrace();
        }
        count = ServiceProxy.count("crm_user");
        System.out.println(count);
        return row.toJSON();
    }
}
