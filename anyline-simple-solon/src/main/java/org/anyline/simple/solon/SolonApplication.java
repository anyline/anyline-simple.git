package org.anyline.simple.solon;

import com.alibaba.druid.pool.DruidDataSource;
import org.anyline.data.datasource.DataSourceHolder;
import org.anyline.data.transaction.TransactionState;
import org.anyline.entity.DataRow;
import org.anyline.metadata.Column;
import org.anyline.metadata.Table;
import org.anyline.proxy.ServiceProxy;
import org.anyline.proxy.TransactionProxy;
import org.noear.solon.Solon;

import java.io.IOException;
import java.util.LinkedHashMap;

public class SolonApplication {
    public static void main(String[] args) throws IOException {
        Solon.start(SolonApplication.class, args);
        //初始货数据库
        init();

        //读取元数据
        metadata();

        //动态 注册/切换数据源
        ds();

        //事务
        tran();

    }
    public static void metadata(){
        Table table = ServiceProxy.metadata().table("CRM_USER");
        System.out.println("表:"+table);
        LinkedHashMap<String, Column> columns = table.getColumns();
        for(Column column:columns.values()){
            System.out.println("列:"+column.getName() + "\t类型:"+column.getFullType());
        }
    }
    public static void tran() {
        try {
            TransactionState state = TransactionProxy.start();
            DataRow row = new DataRow();
            row.put("CODE", "123");
            ServiceProxy.insert("crm_user", row);
            //注意同一个事务(连接)中可以看到非提交的数据，要从另一个连接中(或其他工具)才能看不到未提交的数据
            System.out.println("回滚前:" + ServiceProxy.count("crm_user"));
            TransactionProxy.rollback(state);
            System.out.println("回滚后:" + ServiceProxy.count("crm_user"));

            state = TransactionProxy.start();
            row = new DataRow();
            row.put("CODE", "123");
            ServiceProxy.insert("crm_user", row);
            System.out.println("提交前:" + ServiceProxy.count("crm_user"));
            TransactionProxy.commit(state);
            System.out.println("提交后:" + ServiceProxy.count("crm_user"));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void init(){
        try {
            Table table = ServiceProxy.metadata().table("CRM_USER");
            if (null != table) {
                ServiceProxy.ddl().drop(table);
            }
            table = new Table("CRM_USER");
            table.addColumn("ID", "BIGINT").autoIncrement(true).setPrimary(true).setComment("主键");
            table.addColumn("CODE", "VARCHAR(20)").setComment("编号");
            table.addColumn("NAME", "VARCHAR(20)").setComment("姓名");
            table.addColumn("AGE", "INT").setComment("年龄");
            ServiceProxy.ddl().create(table);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public static void ds(){
        try {
            String key = "d1";
            DruidDataSource ds1 = new DruidDataSource();
            ds1.setUrl("jdbc:mysql://localhost:33306/simple_sso?useUnicode=true&characterEncoding=UTF8&useSSL=false&serverTimezone=Asia/Shanghai&allowPublicKeyRetrieval=true");
            ds1.setDriverClassName("com.mysql.cj.jdbc.Driver");
            ds1.setUsername("root");
            ds1.setPassword("root");
            DataSourceHolder.reg(key, ds1);
            System.out.println("注册数据库成功:" + ds1);


            key = "d2";
            String url = "jdbc:mysql://localhost:33306/simple?useUnicode=true&characterEncoding=UTF8&useSSL=false&serverTimezone=Asia/Shanghai&allowPublicKeyRetrieval=true";
            DataSourceHolder.reg(key, "com.zaxxer.hikari.HikariDataSource", "com.mysql.cj.jdbc.Driver", url, "root", "root");
            System.out.println("注册数据库成功:" + key);
            System.out.println("查询d2表:" + ServiceProxy.service("d2").tables().size());

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
