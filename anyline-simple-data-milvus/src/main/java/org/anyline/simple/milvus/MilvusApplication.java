package org.anyline.simple.milvus;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class MilvusApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(MilvusApplication.class);
        ConfigurableApplicationContext context = application.run(args);
    }
}
