package org.anyline.simple.milvus;

import io.milvus.v2.client.ConnectConfig;
import io.milvus.v2.client.MilvusClientV2;
import io.milvus.v2.service.collection.request.CreateCollectionReq;
import io.milvus.v2.service.collection.request.GetLoadStateReq;
import org.anyline.entity.authorize.Role;
import org.anyline.entity.authorize.User;
import org.anyline.metadata.Table;
import org.anyline.service.AnylineService;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.LinkedHashMap;

@SpringBootTest
public class MilvusTest {
    private static Logger log = LoggerFactory.getLogger(MilvusTest.class);
    @Autowired
    private AnylineService service          ;

    @Test
    public void query() throws Exception{
        LinkedHashMap<String, Table> tables= service.metadata().tables();
        System.out.println(tables);
    }
    @Test
    public void authorize() throws Exception{
        //创建角色
        service.authorize().create(new Role("role_cc"));
        System.out.println(service.authorize().roles());
        //创建用户
        service.authorize().create(new User("user_cc","password_cc"));
        System.out.println(service.authorize().users());

        //调用用户角色
        service.authorize().grant(new User("user_cc"), new Role("role_cc"));

        //删除用户
        service.authorize().drop(new User("user_cc"));

        //删除角色
        service.authorize().drop(new Role("role_cc"));
    }
    @Test
    public void help() throws Exception{ 
    //https://milvus.io/docs/manage-collections.md

        String CLUSTER_ENDPOINT = "http://localhost:39530";

// 1. Connect to Milvus server
        ConnectConfig connectConfig = ConnectConfig.builder()
            .uri(CLUSTER_ENDPOINT)
            .build();

        MilvusClientV2 client = new MilvusClientV2(connectConfig);

// 2. Create a collection in quick setup mode
        CreateCollectionReq quickSetupReq = CreateCollectionReq.builder()
            .collectionName("quick_setup")
            .dimension(5)
            .build();

        client.createCollection(quickSetupReq);

// Thread.sleep(5000);

        GetLoadStateReq quickSetupLoadStateReq = GetLoadStateReq.builder()
            .collectionName("quick_setup")
            .build();

        Boolean res = client.getLoadState(quickSetupLoadStateReq);

        System.out.println(res);
        System.out.println(client.listRoles());

    }
}
