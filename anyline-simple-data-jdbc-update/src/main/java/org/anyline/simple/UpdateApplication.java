package org.anyline.simple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;



@SpringBootApplication

public class UpdateApplication {
	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(UpdateApplication.class);
		ConfigurableApplicationContext context = application.run(args);
	}

}
