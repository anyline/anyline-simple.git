package org.anyline.simple.help;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class PieChartExample {
    public static void main(String[] args) {
        // 创建一个空白的BufferedImage
        BufferedImage image = new BufferedImage(400, 400, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = image.createGraphics();

        // 绘制背景
        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, 400, 400);

        // 绘制饼图
        g2d.setColor(Color.GREEN);
        int radius = 150;
        int x = 200 - radius;
        int y = 200 - radius;
        int width = radius * 2;
        int angle = 90; // 角度
        g2d.fillArc(x, y, width, width, -angle, -angle); // 负角度表示逆时针方向

        // 释放图形上下文资源
        g2d.dispose();

        // 保存图片
        try {
            ImageIO.write(image, "PNG", new File("pie_chart.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 显示图片
        JFrame frame = new JFrame("Pie Chart");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(new JLabel(new ImageIcon("pie_chart.png")));
        frame.pack();
        frame.setVisible(true);
    }
}