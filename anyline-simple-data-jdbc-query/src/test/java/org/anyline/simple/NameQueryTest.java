package org.anyline.simple;

import org.anyline.data.param.ConfigStore;
import org.anyline.data.param.init.DefaultConfigStore;
import org.anyline.proxy.ServiceProxy;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = QueryApplication.class)
public class NameQueryTest {
    @Test
    public void sql(){
        //XML中定义的SQL及查询条件按相同规则处理
        String sql = "SELECT * FROM FI_USER WHERE 1=1 ${AND (ID>:MAX OR ID<:MIN)} AND NAME IS NOT NULL ${AND LVL > :LVL} AND LVL < 20";

        // SELECT * FROM FI_USER WHERE NAME IS NOT NULL AND LVL < 20
        ServiceProxy.querys(sql);

        // SELECT * FROM FI_USER WHERE (ID>? OR ID<?) AND NAME IS NOT NULL AND LVL > ? AND LVL < 20
        ServiceProxy.querys(sql, "MAX:1", "MIN:10", "LVL:20");

        // SELECT * FROM FI_USER WHERE NAME IS NOT NULL AND LVL > ? AND LVL < 20
        ServiceProxy.querys(sql, "MAX:1", "LVL:30");

    }

    @Test
    public void placeholder(){
        //XML中定义的SQL及查询条件按相同规则处理
        String sql = "SELECT * FROM FI_USER WHERE 1=1 AND ID<${MAX} AND ID > #{MIN} order by id";
        ConfigStore configs = new DefaultConfigStore();
        configs.param("MAX", "10");
        configs.param("MIN", "1");
        configs.param("AVG", "5");
        ServiceProxy.querys(sql, configs, "ID = ${AVG}");

    }
    @Test
    public void order(){
        //XML中定义的SQL及查询条件按相同规则处理
        String sql = "SELECT * FROM FI_USER where id> 1 order by id ";

        // SELECT * FROM FI_USER WHERE NAME IS NOT NULL AND LVL < 20
        ServiceProxy.querys(sql, "id>0");

    }

}
