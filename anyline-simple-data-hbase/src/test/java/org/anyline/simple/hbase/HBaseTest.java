package org.anyline.simple.hbase;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

@SpringBootTest
public class HBaseTest {
    public static void main(String[] args) throws IOException {
            Configuration config = HBaseConfiguration.create();
            config.set("hbase.zookeeper.quorum", "localhost");  // HBase ZooKeeper 地址
            config.set("hbase.zookeeper.property.clientPort", "32181");  // HBase ZooKeeper 端口
            Connection con = ConnectionFactory.createConnection(config);
    }

    @Test
    public void namespace() throws Exception{

    }
    @Test
    public void table() throws Exception{

    }
}
