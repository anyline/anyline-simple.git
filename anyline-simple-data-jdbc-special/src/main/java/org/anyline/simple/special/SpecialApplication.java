package org.anyline.simple.special;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication


public class SpecialApplication {


	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(SpecialApplication.class);
		ConfigurableApplicationContext context = application.run(args);
	}
}
