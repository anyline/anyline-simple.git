package org.anyline.simple.interceptor;

import org.anyline.data.interceptor.QueryInterceptor;
import org.anyline.data.param.ConfigStore;
import org.anyline.data.prepare.RunPrepare;
import org.anyline.data.run.Run;
import org.anyline.data.runtime.DataRuntime;
import org.anyline.entity.DataRow;
import org.anyline.entity.DataSet;
import org.anyline.metadata.ACTION;
import org.springframework.stereotype.Component;


@Component
public class DMLQueryInterceptor implements QueryInterceptor {

    @Override
    public ACTION.SWITCH prepare(DataRuntime runtime, String random, RunPrepare prepare, ConfigStore configs, String... conditions) {
        configs.ge("ID", 1);
        return ACTION.SWITCH.CONTINUE;
    }

    @Override
    public ACTION.SWITCH after(DataRuntime runtime, String random, Run run, boolean success, Object result, ConfigStore configs, long millis){
        String sql = run.getFinalQuery();
        if(result instanceof DataRow){
            DataRow row = (DataRow) result;
            row.put("ROW_ID", System.currentTimeMillis());
        }else if(result instanceof DataSet){
            DataSet set = (DataSet) result;
            set.put("ROW_ID", System.currentTimeMillis());
        }
        System.out.println(sql);
        System.out.println(result);
        return ACTION.SWITCH.CONTINUE;
    }


}
