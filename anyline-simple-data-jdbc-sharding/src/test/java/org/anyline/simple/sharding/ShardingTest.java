package org.anyline.simple.sharding;

import org.anyline.metadata.Catalog;
import org.anyline.metadata.Column;
import org.anyline.metadata.Schema;
import org.anyline.metadata.Table;
import org.anyline.service.AnylineService;
import org.anyline.util.BeanUtil;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.LinkedHashMap;

@SpringBootTest
public class ShardingTest {
    private Logger log = LoggerFactory.getLogger(ShardingTest.class);
    @Autowired
    private AnylineService service          ;
    @Autowired
    private JdbcTemplate jdbc               ;
    private Catalog catalog = null          ; // 可以相当于数据库名
    private Schema schema   = null          ; // 如 dbo
    private String table    = "CRM_USER"    ; // 表名

    @Test
    public void metadata(){
        Table table = service.metadata().table("CRM_USER");
        System.out.println(table);
        LinkedHashMap<String, Column> columns = service.metadata("SELECT M.*, F.data_version as DV FROM CRM_USER AS M LEFT JOIN BS_ARRAY AS F ON M.ID = F.ID  WHERE M.ID = :ID ");
        for(Column column:columns.values()){
            System.out.println(BeanUtil.object2json(column));
        }
    }
}
