import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.google.zxing.qrcode.QRCodeWriter;
import org.anyline.adapter.KeyAdapter;
import org.anyline.entity.Aggregation;
import org.anyline.entity.DataRow;
import org.anyline.entity.DataSet;
import org.anyline.entity.OriginRow;
import org.anyline.handler.Downloader;
import org.anyline.handler.Uploader;
import org.anyline.net.HttpUtil;
import org.anyline.office.docx.entity.WDocument;
import org.anyline.office.docx.entity.WTable;
import org.anyline.office.docx.entity.WTc;
import org.anyline.office.docx.entity.WTr;
import org.anyline.office.docx.util.DocxUtil;
import org.anyline.office.util.Imager;
import org.anyline.simple.office.WordApplication;
import org.anyline.util.*;
import org.anyline.util.encrypt.MD5Util;
import org.anyline.util.regular.RegularUtil;
import org.dom4j.Element;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.text.DateFormat;
import java.util.*;

@SpringBootTest(classes = WordApplication.class)
public class TagTest {
    /**
     * 重新组合
     * 合并拆分到多个t中的占位符(包含表达式)和aol标签
     * 拆分标签的前后缀 head body foot到单独的t中
     * 拆分不在标签内的占位符到单独的t中
     */
    @Test
    public void recombine(){
        WDocument doc = doc("ttt.docx");
        List<Element> ts = DocxUtil.contents(doc.getSrc());
        for(Element t:ts){
            System.out.println(t.getText());
        }
        doc.recombine(true);
        System.out.println("======================");
        List<Element> tss = DocxUtil.contents(doc.getSrc());
        for(Element t:tss){
            System.out.println(t.getText());
        }
        System.out.println(DomUtil.format(doc.getSrc()));
    }

    @Test
    public void api() throws Exception {
        String app = "tmp_001"; //应用ID或租户ID
        String secret = "s001"; //密钥
        long timestamp = System.currentTimeMillis(); //当前时间
        //接口地址
        String url = "http://office.api.deepbit.cn/js/ser/docx/parse";

        //模板文件参数名约定用file
        Map<String, Object> files = new HashMap<>();
        files.put("file", new File("E:\\template\\qr.docx"));

        //数据源
        Map<String, Object> params = new HashMap<>();
        DataRow body = new DataRow(KeyAdapter.KEY_CASE.SRC);
        DataSet users = new DataSet();
        for(int i=0; i<10; i++){
            DataRow user = users.add();
            user.put("NAME", "NAME:"+i);
            user.put("CODE", "CODE:"+i);
            user.put("AGE", 20+i);
        }
        body.put("users", users);
        String json = body.toJSON();
        //全部数据格式化成JSON格式放到json参数中
        params.put("json", json);
        //生产环境会有密钥、签名等 也放到params中 测试环境不需要
        params.put("app", app);
        params.put("timestamp", timestamp);
        params.put("sign", sign(json, secret, timestamp));
        String txt = HttpUtil.upload(url, files, params).getText();

        //返回数据的data属性上有生成的docx文件地址
        //如果失败会message属性上会有异常信息
        System.out.println(txt);
    }

    /**
     * 签名 md5(json+secret+time)
     * @param json 完整数据
     * @param secret  密钥
     * @param time 当前时间
     * @return 签名
     */
    private String sign(String json, String secret, long time){
        String sign = null;
        String str = json + secret + time;
        sign = MD5Util.sign(str);
        return sign;
    }
    @Test
    public void test() throws Exception {
        ConfigTable.IS_UPPER_KEY = false;
        String url = "https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png";
        WDocument doc = doc("qr.docx");
        doc.setPlaceholderDefault("/");
         DataSet jsons = new DataSet();
        jsons.add(KeyAdapter.KEY_CASE.SRC).put("url", url);

        //点位符
        doc.replace("a", "100");
        doc.replace("b", "占位值B<br/>");
        doc.replace("c", "200");
        doc.replace("a:b", "abc");;
        doc.replace("money", "12315.23");
        doc.variable("total", 80);
        doc.variable("ymd", new Date());
        DataSet users = new DataSet();
        String[] names = "张一,张二,张三,张四,张五,张六,张七,张八,张九".split(",");
        for(int i=0; i<10; i++){
            DataRow user = users.add(KeyAdapter.KEY_CASE.SRC);
            user.put("NAME", names[i%names.length]);
            user.put("PRICE", 100*i+1);
            user.put("DEPT", i%5);
            user.put("DEPT_NAME", "部门名称"+i%5);
            user.put("CODE", "C"+(i+1));
            user.put("AGE", 20+i);
            user.put("IMG_URL", url);
            user.put("ambient_temperature", "aaaaaaaa"+i);
            user.put("use_record_no", "NO"+i);
            user.put("imgs", jsons.toJSON());
            DataSet urls = DataSet.parseJson("[\n" +
                "    {\n" +
                "        \"url\": \""+url+"\",\n" +
                "        \"name\": \"01.png.biMth0w7zboFd4J7ybq1EPr5F313mAGUAp3Vc7thQV8bBNcrE8S_AFPzJ7_i59rNll26BAbtXeD8d6MqIhyYXw\",\n" +
                "        \"type\": \"biMth0w7zboFd4J7ybq1EPr5F313mAGUAp3Vc7thQV8bBNcrE8S_AFPzJ7_i59rNll26BAbtXeD8d6MqIhyYXw\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"url\": \""+url+"\",\n" +
                "        \"name\": \"02.png.biMth0w7zboFd4J7ybq1EPr5F313mAGUAp3Vc7thQV8bBNcrE8S_AFPzJ7_i59rNll26BAbtXeD8d6MqIhyYXw\",\n" +
                "        \"type\": \"biMth0w7zboFd4J7ybq1EPr5F313mAGUAp3Vc7thQV8bBNcrE8S_AFPzJ7_i59rNll26BAbtXeD8d6MqIhyYXw\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"url\": \""+url+"\",\n" +
                "        \"name\": \"03.png.biMth0w7zboFd4J7ybq1EPr5F313mAGUAp3Vc7thQV8bBNcrE8S_AFPzJ7_i59rNll26BAbtXeD8d6MqIhyYXw\",\n" +
                "        \"type\": \"biMth0w7zboFd4J7ybq1EPr5F313mAGUAp3Vc7thQV8bBNcrE8S_AFPzJ7_i59rNll26BAbtXeD8d6MqIhyYXw\"\n" +
                "    }\n" +
                "]");
            user.put("urls", urls);
        }
        doc.variable("users", users);
        DataSet set = new DataSet();
        for( int i=0; i<10; i++){
            String str = "[{\"t_sample__receiver_time\":\"2025-01-09 18:41:28\",\"t_detection_task_report_detail__task_start_date\":\"2025-01-09 18:41:28\",\"t_detection_task_report_detail__task_end_date\":\"2025-01-09 18:41:28\",\"t_detection_task_add_detail__testing_basis\":\"依据5\",\"t_detection_task_add_detail__testing_item_name\":\"依据name5\",\"_detection_task_add_detail__judgment_basis\":\"判定5\",\"t_detection_report_detail__conclusion\":30230,\"t_detection_report_detail__testing_item_name\":\"方法2\",\"t_sample__sample_specifications\":\"规格:5\",\"t_sample__production_unit_name\":\"厂家:5\",\"time\":\"time:5\",\"t_hydrostatic_test__results\":\"结果2\",\"t_template_check_device_underwater__device_no\":\"no5\",\"notes\":\"notes:5\",\"mpa\":\"mpa:5\",\"t_detection_report_detail__sample_numberName\":\"name:5\",\"t_detection_report_detail__testing_item\":null,\"imgs\":[{\"url\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\"}],\"t_hydrostatic_test_detail__time\":\"2025-01-09 18:41:28\",\"t_entrust__customer_address\":{\"selectLabel\":\"selectLabel5\",\"inputValue\":\"inputValue5\"},\"t_entrust__customer_contact\":\"张三\",\"t_entrust__customer_contact_phone\":\"053212345678\",\"t_sample__sample_number\":\"NO-2\",\"t_detection_report_detail__testing_basis\":\"依据2\",\"t_entrust__issuing_officeName\":\"委托单位5\",\"t_sample__sample_name\":\"name:5\",\"t_sample__sample_statusName\":\"状态:5\",\"t_template_check_device_underwater__device_name\":\"设备5\",\"t_detection_report_detail__sample_name\":\"sample2\",\"t_template_check_device_underwater__id\":\"device2\",\"t_template_check_device_underwater__next_verification_date\":\"2025-01-09\",\".t_hydrostatic_test_detail__time\":\"2025-01-09 18:41:28\",\"t_hydrostatic_test_detail__mpa\":\"1000\",\"t_hydrostatic_test_detail__notes\":\"notes\",\"device_no\":\"MAQ-A-0\",\"device_name\":\"name0\",\"ambient_temperature\":\"temp5\",\"t_hydrostatic_test__test_photo_upload\":[{\"url\":\"/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"name\":\"01.png.biMth0w7zboFd4J7ybq1EPr5F313mAGUAp3Vc7thQV8bBNcrE8S_AFPzJ7_i59rNll26BAbtXeD8d6MqIhyYXw\",\"type\":\"biMth0w7zboFd4J7ybq1EPr5F313mAGUAp3Vc7thQV8bBNcrE8S_AFPzJ7_i59rNll26BAbtXeD8d6MqIhyYXw\"},{\"url\":\"/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"name\":\"02.png.biMth0w7zboFd4J7ybq1EPr5F313mAGUAp3Vc7thQV8bBNcrE8S_AFPzJ7_i59rNll26BAbtXeD8d6MqIhyYXw\",\"type\":\"biMth0w7zboFd4J7ybq1EPr5F313mAGUAp3Vc7thQV8bBNcrE8S_AFPzJ7_i59rNll26BAbtXeD8d6MqIhyYXw\"},{\"url\":\"/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"name\":\"03.png.biMth0w7zboFd4J7ybq1EPr5F313mAGUAp3Vc7thQV8bBNcrE8S_AFPzJ7_i59rNll26BAbtXeD8d6MqIhyYXw\",\"type\":\"biMth0w7zboFd4J7ybq1EPr5F313mAGUAp3Vc7thQV8bBNcrE8S_AFPzJ7_i59rNll26BAbtXeD8d6MqIhyYXw\"}]}]";
           //set.addAll(DataSet.parseJson(str));
            DataRow row = set.add(KeyAdapter.KEY_CASE.SRC);

            row.put("t_sample__receiver_time", DateUtil.format());
            row.put("t_detection_task_report_detail__task_start_date", DateUtil.format());
            row.put("t_detection_task_report_detail__task_end_date", DateUtil.format());
            row.put("t_detection_task_add_detail__testing_basis", "依据"+ i);
            row.put("t_detection_task_add_detail__testing_item_name", "依据name"+ i);
            row.put("_detection_task_add_detail__judgment_basis", "判定"+i);
            row.put("t_detection_report_detail__conclusion", 30230);
            row.put("t_detection_report_detail__testing_item_name", "name"+i);
            row.put("t_sample__sample_specifications", "规格:"+i);
            row.put("t_sample__production_unit_name", "厂家:"+i);
            row.put("time", "time:"+i);
            row.put("t_hydrostatic_test__results", "结果"+i%3);
            row.put("t_template_check_device_underwater__device_no", "no"+i);
            row.put("notes", "notes:"+i);
            row.put("mpa", "mpa:"+i);
            row.put("t_detection_report_detail__sample_numberName", "name:"+i);
            row.put("t_detection_report_detail__testing_item", null);
            row.put("imgs", jsons.toJSON());
            row.put("t_hydrostatic_test_detail__time", new Date());
            DataRow adr = new OriginRow();
            adr.put("selectLabel", "selectLabel"+i);
            adr.put("inputValue", "inputValue"+i);
            row.put("t_entrust__customer_address", adr.toJSON());
            row.put("t_entrust__customer_contact", "张三");
            row.put("t_entrust__customer_contact_phone", "053212345678");
            row.put("t_sample__sample_number", "NO-"+i%3);
            row.put("t_detection_report_detail__testing_basis", "依据"+i%3);
            row.put("t_detection_report_detail__testing_item_name", "方法"+i%3);
            row.put("t_sample__receiver_time", DateUtil.format());
            row.put("t_entrust__issuing_officeName", "委托单位"+i);
            row.put("t_sample__sample_name", "name:"+i);
            row.put("t_sample__sample_statusName", "状态:"+i);
            row.put("t_template_check_device_underwater__device_name", "设备"+i);
            row.put("t_detection_report_detail__sample_name", "sample"+i%3);
            row.put("t_template_check_device_underwater__id", "device"+i%3);
            row.put("t_template_check_device_underwater__next_verification_date", DateUtil.format("yyyy-MM-dd"));
            row.put(".t_hydrostatic_test_detail__time", DateUtil.format());
            row.put("t_hydrostatic_test_detail__mpa", "1000");
            row.put("t_hydrostatic_test_detail__notes", "notes");
            row.put("device_no", "MAQ-A-"+i%2);
            row.put("device_name","name"+i);
            row.put("submit_person", "temp"+i);
            row.put("t_detection_task_distribute__log_template", "TMP"+i%3);
            row.put("t_template_check_device__device_name", "设备名"+i);
           /* String urls ="[\n" +
                "    {\n" +
                "        \"url\": \"/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\n" +
                "        \"name\": \"01.png.biMth0w7zboFd4J7ybq1EPr5F313mAGUAp3Vc7thQV8bBNcrE8S_AFPzJ7_i59rNll26BAbtXeD8d6MqIhyYXw\",\n" +
                "        \"type\": \"biMth0w7zboFd4J7ybq1EPr5F313mAGUAp3Vc7thQV8bBNcrE8S_AFPzJ7_i59rNll26BAbtXeD8d6MqIhyYXw\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"url\": \"/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\n" +
                "        \"name\": \"02.png.biMth0w7zboFd4J7ybq1EPr5F313mAGUAp3Vc7thQV8bBNcrE8S_AFPzJ7_i59rNll26BAbtXeD8d6MqIhyYXw\",\n" +
                "        \"type\": \"biMth0w7zboFd4J7ybq1EPr5F313mAGUAp3Vc7thQV8bBNcrE8S_AFPzJ7_i59rNll26BAbtXeD8d6MqIhyYXw\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"url\": \"/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\n" +
                "        \"name\": \"03.png.biMth0w7zboFd4J7ybq1EPr5F313mAGUAp3Vc7thQV8bBNcrE8S_AFPzJ7_i59rNll26BAbtXeD8d6MqIhyYXw\",\n" +
                "        \"type\": \"biMth0w7zboFd4J7ybq1EPr5F313mAGUAp3Vc7thQV8bBNcrE8S_AFPzJ7_i59rNll26BAbtXeD8d6MqIhyYXw\"\n" +
                "    }\n" +
                "]";
            row.put("t_hydrostatic_test__test_photo_upload", urls);*/

        }
        doc.variable("fileServer", "https://www.baidu.com");
        //<aol:for var="item" items="${}" status="s" scope="tr"/>${s.count}
        doc.variable("t_detection_report_detail", set);
        doc.variable("master", set);

        //图片
        //在word模板中添加内容   <aol:img src="${FILE_URL_COL}" style="width:150px;height:${LOGO_HEIGHT}px;"></aol:img>
        doc.replace("FILE_URL_COL", url);
        doc.replace("LOGO_HEIGHT", "100");
        DataRow row = new DataRow();
        row.put("FILE_URL_COL", url);
        doc.variable("row", row);
        List<String> imgs = new ArrayList<>();
        imgs.add(url);
        imgs.add(url);
        doc.variable("imgs", imgs);
        //日期格式化
        //<aol:date format="yyyy-MM-dd HH:mm:ss" value="${current_time}"></aol:date>
        doc.replace("current_time", DateUtil.format());

        //数字格式化
        //<aol:number format="###,##0.00" value="${total}"></aol:number>
        doc.replace("total", "123456789");

        //复选
        DataSet depts = new DataSet();
        depts.add().set("ID", 0).set("NAME", "一班").set("CODE", "A1");
        depts.add().set("ID", 1).set("NAME", "二班").set("CODE", "A2");
        depts.add().set("ID", 2).set("NAME", "三班").set("CODE", "A3");
        depts.add().set("ID", 3).set("NAME", "四班").set("CODE", "A4");
        depts.add().set("ID", "A").set("NAME", "五班").set("CODE", "A5");

        doc.variable("depts", depts);
        row.put("depts", depts);
        for(DataRow dept:depts){
            DataSet usrs = new DataSet();
            dept.put("USERS", usrs);
            for(int i=1; i<3; i++) {
                String json = "{'url':'https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png'}";
                DataRow usr = usrs.add();
                usr.set("NAME", "name"+i).set("SCORE", i*100).set("IMG", json);
                DataSet scores = new DataSet();
                for(int j=1; j<=3; j++){
                    DataRow score = scores.add();
                    score.put("SCORE", j*10);
                    score.put("NAME", "第"+j+"次考试");
                }
                usr.put("SCORES", scores);
            }
        }
        //固定单选
        //<aol:checkbox value="1" data="${depts}"></aol:checkbox>

        //固定多选
        //每行两个选项
        //<aol:checkbox value="{1,2}" vol="2" data="${depts}"></aol:checkbox>

        //动态多选
        //根据ID列值判断是否选中
        //<aol:checkbox value="${chks}" rely="ID" vol="2" data="${depts}"></aol:checkbox>
        DataSet chks = new DataSet();
        chks.add().put("ID", 0);
        chks.add().put("ID", "A");
        doc.variable("chks", chks);

        //预宝标签
        doc.predefine("d", "<aol:date pre='d' value='${ymd}' format='yyyy-MM-dd HH:mm:ss'/>");
        doc.predefine("chka", "<aol:checkbox  rely='ID' data='${depts}'/> ");


        //多列遍历
        DataSet items = new DataSet();
        DataRow it = items.add();
        it.put("NAME", "A1");
        it.put("V_MIN", "0.1");
        it.put("V_MAX", "0.11");
        it = items.add();
        it.put("NAME", "A2");
        it.put("V_MIN", "0.2");
        it.put("V_MAX", "0.22");
        it = items.add();
        it.put("NAME", "A3");
        it.put("V_MIN", "0.3");
        it.put("V_MAX", "0.33");


        doc.variable("items", items);


        doc.save();

    }
    private WDocument doc(){
        return doc("tag_trs.docx");
    }
    private WDocument doc(String name){
        File file = new File("E:\\template\\"+name);
        File copy = new File("E:\\template\\result\\tag_"+System.currentTimeMillis()+".docx");
        FileUtil.copy(file, copy);
        WDocument doc = new WDocument(copy);
        doc.setDownloader(new Downloader() {
            @Override
            public boolean download(String url, File file) {
                url = HttpUtil.encode(url, true, true);
                HttpUtil.download(url, file);
                return true;
            }
        });
        doc.setImager(new Imager() {
            @Override
            public File qr(String content, int width, int height) {
                File file = new File("qr/"+BasicUtil.getRandomLowerString(10)+".jpg");
                File dir = file.getParentFile();
                if(!dir.exists()){
                    dir.mkdirs();
                }
                try {
                    QRCodeWriter qrCodeWriter = new QRCodeWriter();
                    Map<EncodeHintType, Object> hints = new HashMap<>();
                    hints.put(EncodeHintType.MARGIN, 0); // 设置边距
                    hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");  // 设置字符编码为UTF-8
                    BitMatrix bitMatrix = qrCodeWriter.encode(content, BarcodeFormat.QR_CODE, width, height, hints);
                    Path path = FileSystems.getDefault().getPath(file.getAbsolutePath());
                    MatrixToImageWriter.writeToPath(bitMatrix, "JPEG", path);

                }catch (Exception e){
                    e.printStackTrace();
                }
                return file;
            }

            @Override
            public File bar(String content, int width, int height) {
                File file = new File("bar/"+BasicUtil.getRandomLowerString(10)+".jpg");
                try {
                    File dir = file.getParentFile();
                    if (!dir.exists()) {
                        dir.mkdirs();
                    }
                    // 创建条形码生成器对象，这里以Code128为例
                    Code128Writer barcodeWriter = new Code128Writer();
                    // 设置编码的参数，这里可以设置字符集等参数，例如：UTF-8编码
                    Map<EncodeHintType, Object> hints = new HashMap<>();
                    hints.put(EncodeHintType.MARGIN, 0); // 设置边距
                    hints.put(EncodeHintType.CHARACTER_SET, "UTF-8"); // 设置字符集为UTF-8，支持中文等字符
                    // 生成条形码矩阵，该方法返回一个BitMatrix对象，其中true表示黑色，false表示白色
                    BitMatrix bitMatrix = barcodeWriter.encode(content, BarcodeFormat.CODE_128, width, height, hints);
                    // 将BitMatrix对象写入到文件中，这里以PNG格式为例
                    Path path = FileSystems.getDefault().getPath(file.getAbsolutePath());
                    MatrixToImageWriter.writeToPath(bitMatrix, "JPEG", path); // 注意：这里的路径要和你的项目路径相对应或者使用绝对路径
                }catch (Exception e){
                    e.printStackTrace();
                }
                return file;
            }
        });
        return doc;
    }
}
