package org.anyline.simple.entity;

import org.anyline.adapter.KeyAdapter;
import org.anyline.data.param.ConfigStore;
import org.anyline.data.param.init.DefaultConfigStore;
import org.anyline.data.run.Run;
import org.anyline.entity.DataRow;
import org.anyline.metadata.Table;
import org.anyline.proxy.ServiceProxy;
import org.anyline.service.AnylineService;
import org.anyline.util.BeanUtil;
import org.anyline.util.ConfigTable;
import org.anyline.util.DateUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest(classes = EntityApplication.class)
public class EntityTest {

    @Autowired
    private AnylineService service;

    @Test
    public void entity(){
        ConfigTable.IS_AUTO_CHECK_METADATA = true;
        Employee employee = new Employee();
        employee.setJoinYmd(DateUtil.format());
        DataRow row = DataRow.parse(KeyAdapter.KEY_CASE.CAMEL_SRC, employee);
        Boolean f = false;
        row.put("IS_BIT",f);
        ServiceProxy.save("CRM_USER", row);

        ConfigStore configs = new DefaultConfigStore();
        configs.and("CODE", "1");
        ServiceProxy.update("CRM_USER", row, configs);
    }
    @Test
    public void col(){
        String tableName = "HR_EMPLOYEE";
        Class clz = Employee.class;
        ConfigStore configs = new DefaultConfigStore();
        ServiceProxy.service().selects(tableName, clz, configs);
    }
    @Test
    public void json(){
        ConfigTable.IS_THROW_CONVERT_EXCEPTION = true;
        Employee employee = ServiceProxy.select(Employee.class);
        System.out.println(BeanUtil.object2json(employee.getExt()));
    }
    @Test
    public void pk(){
        Department dept = new Department();
        dept.setId(11L);
        dept.setName("A");
        ConfigStore configs = new DefaultConfigStore();
        service.update("HR_DEPARTMENT<code>",dept, configs,"name");
        List<Run> runs = configs.runs();
        for (Run run:runs){
            System.out.println(run.getFinalUpdate(false));
        }
        DataRow row = new DataRow(KeyAdapter.KEY_CASE.SRC);
        row.put("ID", "1");
        row.put("code", "2");
        row.put("name", "2n");
        service.update("HR_DEPARTMENT<code>", row,  "name");
    }
    @Test
    public void query() throws Exception{
        ServiceProxy.select(Employee.class, "id:1");
    }
    @Test
    public void table() throws Exception{
        Table table = Table.from(Employee.class);
        if(service.metadata().exists(table)){
            service.ddl().drop(table);
        }
        table.execute(false);
        service.ddl().create(table);
        System.out.println(table.ddls());
    }
    @Test
    public void stringJson() throws Exception{
        Employee employee = ServiceProxy.select("hr_employee", Employee.class);
        System.out.println(employee.getStringJson());
    }
    @Test
    public void camel_() throws Exception{
        //ConfigTable.IS_AUTO_CHECK_METADATA = true;
        //默认驼峰转下划线
        Employee employee = new Employee();
        employee.setNm("z");
        //service.insert(employee);
        //如果不需要 请修改这个属性成null 注意这里会有缓存 上一步转换一次了 这一步会用上一步的缓存 不会重新转换
        ConfigTable.ENTITY_FIELD_COLUMN_MAP = null;
        employee = new Employee();
        employee.setNm("z");
        service.insert(employee);
    }

    @Test
    public void testField() throws Exception {
        ConfigTable.IS_AUTO_CHECK_METADATA = true;
        Employee employee = new Employee();
        employee.setAge(20);
        employee.setStreamURL("http");
        service.save(employee);
        employee.setStreamURL("http2");
        service.save(employee);
    }
}
