package org.anyline.simple.entity;

import org.anyline.entity.DataRow;
import org.anyline.entity.OriginRow;

import java.util.Date;

public class Post {
    private String name;
    private double perk;
    private Date date;
    private String boxInCMax;

    public static void main(String[] args) {
        DataRow row = new OriginRow();
        row.put("box_in_c_max","123");
        Post post = row.entity(Post.class);
        System.out.println(row);
        System.out.println(post.boxInCMax);
    }

    public Post(){

    }
    public Post(String name, double perk, Date date){
        this.name = name;
        this.perk = perk;
        this.date = date;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPerk() {
        return perk;
    }

    public void setPerk(double perk) {
        this.perk = perk;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
