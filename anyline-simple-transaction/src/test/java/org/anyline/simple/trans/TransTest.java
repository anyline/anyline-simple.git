package org.anyline.simple.trans;

import org.anyline.data.datasource.DataSourceHolder;
import org.anyline.data.transaction.TransactionState;
import org.anyline.proxy.ServiceProxy;
import org.anyline.proxy.TransactionProxy;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TransTest {
    /*
     * 两个数据源 同时启动事务  保持隔离
     */
    @Test
    public void test() throws Exception {

        String url = "jdbc:mysql://localhost:33306/simple_sso?useUnicode=true&characterEncoding=UTF8&useSSL=false&serverTimezone=Asia/Shanghai&allowPublicKeyRetrieval=true";
        DataSourceHolder.reg("sso", "com.zaxxer.hikari.HikariDataSource", "com.mysql.cj.jdbc.Driver", url, "root", "root");

        url = "jdbc:mysql://localhost:33306/simple_crm?useUnicode=true&characterEncoding=UTF8&useSSL=false&serverTimezone=Asia/Shanghai&allowPublicKeyRetrieval=true";
        DataSourceHolder.reg("crm", "com.zaxxer.hikari.HikariDataSource", "com.mysql.cj.jdbc.Driver", url, "root", "root");

        TransactionState state_sso = TransactionProxy.start("sso");
        ServiceProxy.service("sso").delete("sso_user", "id", "1");

        TransactionState state_crm = TransactionProxy.start("crm");
        ServiceProxy.service("crm").delete("crm_user", "id", "1");

        TransactionProxy.rollback(state_crm);
        TransactionProxy.rollback(state_sso);
    }

}
