package org.anyline.simple.mongodb;

import org.anyline.data.datasource.DataSourceHolder;
import org.anyline.data.mongodb.entity.MongoRow;
import org.anyline.data.param.ConfigStore;
import org.anyline.data.param.init.DefaultConfigStore;
import org.anyline.entity.Compare;
import org.anyline.entity.DataSet;
import org.anyline.entity.DefaultPageNavi;
import org.anyline.entity.PageNavi;
import org.anyline.metadata.Database;
import org.anyline.metadata.Schema;
import org.anyline.metadata.Table;
import org.anyline.proxy.ServiceProxy;
import org.anyline.service.AnylineService;
import org.anyline.util.ConfigTable;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

@SpringBootTest
public class MongoTest {
    @Test
    public void ds() throws Exception {

        String url = "mongodb://localhost:27017";
        Map<String, Object> params = new HashMap<>();
        params.put("url", url);
        params.put("database", "hr");

        DataSourceHolder.reg("hr2", params);
        String mg = DataSourceHolder.reg("hr2", params, true);

        DataSet set = new DataSet();
        long fr = System.currentTimeMillis();
        for(int i=0; i<10;i ++){
            MongoRow row = new MongoRow();
            row.put("_id", fr+i);
            row.put("name", "USER_"+i);
            row.put("code"+i, "CODE"+i);
            row.put("age",i);
            set.add(row);
        }
        //ServiceProxy.service("hr2").truncate("crm_user6");
        ServiceProxy.service("hr2").insert("crm_user6", set);
        List<Map<String, Object>> list = new ArrayList<>();

        for(int i=0; i<10;i ++){
            Map<String, Object> row = new HashMap<>();
            row.put("name", "USER_"+i);
            row.put("code"+i, "CODE"+i);
            row.put("age",i);
            list.add(row);
        }

        ServiceProxy.service("hr2").insert("crm_user6", list);
        LinkedHashMap<String, Table> tables = ServiceProxy.service("hr2").metadata().tables();
        System.out.println(tables);
        LinkedHashMap<String, Database> databases = ServiceProxy.service("hr2").metadata().databases();
        LinkedHashMap<String, Schema> schemas = ServiceProxy.service("hr2").metadata().schemas();
        System.out.println(databases);
        System.out.println(schemas);

        set = ServiceProxy.service("hr2").querys("crm_user6(name,age)");

        System.out.println(set);
        ConfigStore configs = new DefaultConfigStore();
        configs.and("_id", set.get(0,"_id"));
        System.out.println(
            ServiceProxy.service("hr2").query("crm_user6", configs)
        );
        System.out.println(
            ServiceProxy.service("hr2").query("crm_user6", "_id:"+set.get(0,"_id"))
        );
    }
    @Test
    public void update(){
        Map<String, Object> row = new HashMap<>();
        row.put("CODE","1");
        ServiceProxy.update("table", row, new DefaultConfigStore().eq("_id", 1));

        HashMap<String, Object> map1 = new HashMap<>(16);
        map1.put("data", new ArrayList<>());
        ServiceProxy.update("table", map1, new DefaultConfigStore().eq("cid", "3bc76ee7f79f4b7fbf88bb9b23f6ed96"), "data");
    }
    @Test
    public void query(){
        ConfigStore configs = new DefaultConfigStore();
        configs.limit(2, 30);
        ServiceProxy.querys("table(name,age)", configs,"NAME:1", "CODE:2", "TYPE:3");
    }
    @Test
    public void init(){
        String table =  "crm_user6";
        ConfigTable.PRIMARY_GENERATOR_SNOWFLAKE_ACTIVE = true;
        AnylineService service = ServiceProxy.service("hr");
        DataSet set = new DataSet();
        for(int i=0; i<10;i ++){
            MongoRow row = new MongoRow();
            //row.put("_id", fr+i);
            row.put("name", "USER_"+i);
            row.put("code", "CODE"+i);
            row.put("age",i);
            set.add(row);
        }
        MongoRow row = new MongoRow();
        //row.put("_id", fr+i);
        row.put("name", "USER_"+1);
        row.put("code", "CODE"+1);
        row.put("age",1);
        ConfigTable.DEFAULT_MONGO_ENTITY_CLASS = MongoRow.class;
        //insert时 _id不可以重复
        //插入多行
        service.insert(table, set);
        Assertions.assertEquals(10, set.size());
        row = (MongoRow) service.query(table);

        if(null == row){
            row = new MongoRow();
        }
        //更新一行 根据_id
        row.put("name", "test");
        service.save(table, row);

        row = (MongoRow) service.query(table ,"name:test");
        System.out.println("修改结果:"+row);

        //根据条件更新
        service.update(table, row, new DefaultConfigStore().and(Compare.GREAT, "age", 1));
        System.out.println(service.querys(table));
        service.delete(row);

        service.delete(table, "_id", "1");

        service.deletes(table, "code", "CODE1","CODE2");

        //查询整个表
        set = service.querys(table);
        //查部分列 主键_id默认查询
        set = service.querys("crm_user2(age,code)");
        System.out.println("查询结果:"+set.size());
        System.out.println("查询结果结构:"+set.getRow(0));
        //不查AGE和_id
        set = service.querys("crm_user2(!age,!_id)");

        System.out.println("查询结果:"+set.size());
        if(set.size()>0) {
            System.out.println("查询结果结构:" + set.getRow(set.size() - 1));
        }
        //按条件查询
        set = service.querys(table,
                new DefaultConfigStore().and(Compare.GREAT,"age", 1)
                        .and(Compare.LESS_EQUAL, "age", 5)
                        .in("age", new Integer[]{1,2})
                ,"name:%1%","code:%1");

        System.out.println("查询结果:"+set.size());
        if(!set.isEmpty()) {
            System.out.println("查询结果结构:" + set.getRow(set.size() - 1));
        }
        //分页
        set = service.querys(table, new DefaultPageNavi(2).setPageRows(2));
        PageNavi navi = set.getNavi();
        System.out.println("查询结果: 总行数="+navi.getTotalRow()+",总页数:"+navi.getTotalPage()+",当前页:"+navi.getCurPage()+",结果集:"+set);

        ConfigStore delete = new DefaultConfigStore();
        delete.and("age", 1);
        service.delete(table, delete);
    }
}
