package org.anyline.simple.stream;

import org.anyline.data.handler.DataRowHandler;
import org.anyline.data.handler.EntityHandler;
import org.anyline.data.handler.MapHandler;
import org.anyline.data.handler.ResultSetHandler;
import org.anyline.data.jdbc.handler.SimpleResultSetHandler;
import org.anyline.data.param.ConfigStore;
import org.anyline.data.param.init.DefaultConfigStore;
import org.anyline.entity.DataRow;
import org.anyline.entity.DataSet;
import org.anyline.proxy.ServiceProxy;
import org.anyline.util.BeanUtil;
import org.anyline.util.DateUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest(classes = StreamApplication.class)
public class StreamTest {
    
    private void init(){
        long fr = System.currentTimeMillis();
        DataSet set = new DataSet();
        for(int i=0; i<500; i++){
            DataRow row = new DataRow();
            row.put("ID", fr+i);
            row.put("CODE", "C"+i);
            row.put("NAME", "N"+i);
            set.add(row);
        }
        ServiceProxy.insert("CRM_USER", set);
    }
    @Test
    public void maps(){
        init();
        MapHandler handler = new MapHandler() {
            @Override
            public boolean read(Map<String, Object> map) {
                System.out.println(map);
                return false;
            }
        };
        ServiceProxy.maps("CRM_USER", handler, "ID IN(1,2,3)");

        //或者放到 ConfigStore中
        ConfigStore configs = new DefaultConfigStore();
        configs.handler(handler);
        configs.and("CODE IS NOT NULL");

        ServiceProxy.maps("CRM_USER", configs, "ID IN(1,2,3)");
    }
    @Test
    public void tidb2oceanbase() throws Exception {
        List<Map> maps = new ArrayList<>();
        ConfigStore configs = new DefaultConfigStore().IS_LOG_SQL(false).IS_LOG_SQL_PARAM(false);
        MapHandler handler = new MapHandler() {
            @Override
            public boolean read(Map<String, Object> map) {
                maps.add(map);
                if(maps.size() > 200){
                    ServiceProxy.service("oceanbase").insert("dwd_iot_vehicle_info", maps, configs);
                    maps.clear();
                }
                return true;
            }
        };
        //查出目录库中最大时间
        DataRow max = ServiceProxy.service("oceanbase").query("dwd_iot_vehicle_info", "order by ts desc");
        if(null != max){
            //删除目标库中与最大时间 相等的数据
            String time = DateUtil.format(max.getDate("ts"), "yyyy-MM-dd HH:mm:ss");
            ServiceProxy.service("oceanbase").execute("delete from dwd_iot_vehicle_info where ts ='"+time+"'");
            ServiceProxy.service("tidb").maps("dwd_iot_vehicle_info", handler, "TS>='"+time+"'");
        }else{
            ServiceProxy.service("tidb").maps("dwd_iot_vehicle_info", handler);
        }
    }
    @Test
    public void row(){
        init();

        DataRowHandler handler = new DataRowHandler() {
            @Override
            public boolean read(DataRow row) {
                System.out.println(row);
                return true;
            }
        };
        ServiceProxy.querys("CRM_USER", handler, "ID:>1");
    }
    @Test
    public void entity(){
        init();
        EntityHandler<User> handler = new EntityHandler<User>() {
            @Override
            public boolean read(User user) {
                System.out.println(BeanUtil.object2json(user));
                return false;
            }

        };
        ServiceProxy.selects("CRM_USER", User.class, handler, "ID:>1");
    }
    @Test
    public void resultSet(){
        init();
        ResultSetHandler handler = new ResultSetHandler() {
            @Override
            public boolean read(ResultSet set) {
                try {
                    System.out.println(set.getObject(1));
                }catch (Exception e){
                    e.printStackTrace();
                }
                return true;
            }
        };
        ServiceProxy.querys("CRM_USER", handler, "ID:>1");
    }

    @Test
    public void simple() throws Exception{
        SimpleResultSetHandler handler = new SimpleResultSetHandler();
        ServiceProxy.maps("CRM_USER", handler, "ID:>1");
        while (true) {
            LinkedHashMap<String, Object> map = handler.map();
            if (null == map) {
                break;
            }
            System.out.println(map);
        }
        //如果中途break了要调用handler.close()释放连接
        //如果一直读取到最后，读到map ==null了handler.close()会自动调用
        handler.close();
    }

}
