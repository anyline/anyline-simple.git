package org.anyline.simple.service;


import org.anyline.data.transaction.TransactionState;
import org.anyline.entity.DataRow;
import org.anyline.proxy.ServiceProxy;
import org.anyline.proxy.TransactionProxy;
import org.anyline.service.AnylineService;
import org.springframework.stereotype.Component;

@Component("ds.service")
public class DSService {

    //通用数据源(就是可以执行多个数据源，可切换)
    public void insert(String ds, DataRow row) throws Exception{
        TransactionState status = TransactionProxy.start(ds);//这里没有指定数据源 会默认当前数据源
        try {
            ServiceProxy.service(ds).insert("SSO_USER", row);
            TransactionProxy.commit(status);//这里不需要指定数据源，因为status已经绑定了数据源
        }catch (Exception e){
            TransactionProxy.rollback(status);
        }
    }

    //固定数据源(就是为每个数据源生成独立的jdbcTemplate,事务管理器,dao,service)
    //推荐这种方式，可以避免线程共享的引起的切换失败问题
    public void inserts(String ds, DataRow row) throws Exception{
        AnylineService service = ServiceProxy.service(ds); //返回操作固定数据源的service
        TransactionState status = TransactionProxy.start(ds);
        try {
            service.insert("SSO_USER", row);
            TransactionProxy.commit(status); //这里不需要指定数据源，因为status已经绑定了数据源
        }catch (Exception e){

            TransactionProxy.rollback(status);
        }
    }
}
