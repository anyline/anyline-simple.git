package org.anyline.simple.service;


import com.zaxxer.hikari.HikariDataSource;
import org.anyline.data.param.init.DefaultConfigStore;
import org.anyline.data.runtime.RuntimeHolder;
import org.anyline.data.transaction.TransactionState;
import org.anyline.entity.DataRow;
import org.anyline.proxy.CacheProxy;
import org.anyline.proxy.ServiceProxy;
import org.anyline.proxy.TransactionProxy;
import org.anyline.service.AnylineService;
import org.anyline.util.BasicUtil;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component("sso.service")
public class SSOService {

    @Transactional(value = "anyline.transaction.sso")
    public void insert(DataRow row){
        try {
            ServiceProxy.service("sso").insert("SSO_USER", row);
        }catch (Exception e){

        }
        throw new RuntimeException("test exception");
    }
    public void test(String ds) throws Exception{
        AnylineService<?> service = ServiceProxy.service(ds);
        TransactionState status = TransactionProxy.start(ds);

        JdbcTemplate jdbc = (JdbcTemplate) RuntimeHolder.runtime("sso").getProcessor();
        HikariDataSource dataSource = (HikariDataSource ) jdbc.getDataSource();
        try {
            CacheProxy.clear();
            service.metadata().table("sso_user");
            DataRow row = service.query("sso_user");
            row.put("NM", BasicUtil.getRandomString(10));
            long updateResut = service.update("sso_user", row, new DefaultConfigStore());

            System.out.println("---------------活动(未释放)："+dataSource.getHikariPoolMXBean().getActiveConnections());
            System.out.println("---------------空闲(可用)："+dataSource.getHikariPoolMXBean().getIdleConnections());
            TransactionProxy.commit(status);

        } catch (Exception e) {
            //回滚事务
            TransactionProxy.rollback(status);
        }
        System.out.println("================活动(未释放)："+dataSource.getHikariPoolMXBean().getActiveConnections());
        System.out.println("================空闲(可用)："+dataSource.getHikariPoolMXBean().getIdleConnections());
    }
}
